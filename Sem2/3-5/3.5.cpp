//Найти максимальный элемент массива (целых чисел)
#include <iostream>
using namespace std;
int main () {
    int max, tmp, n=5;
    int arr[n];
    cin >> tmp;
    max = tmp;
    arr[0] = tmp;
    for (int i = 2; i <= n; i++){
        cin >> tmp;
        arr[i] = tmp;
        if (arr[i] > max) {
            max = arr[i];
        }
    }
    cout << "Max element: " << max;
    return 0;
}