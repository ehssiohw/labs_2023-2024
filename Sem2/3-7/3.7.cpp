//За один просмотр (т.е. один цикл) найти и минимальный, и максимальный элемент массива (массив целых чисел).
#include <iostream>
using namespace std;
int main () {
    int max, min, tmp, n=5, k=0;
    int arr[n];
    cin >> arr[0];
    max = arr[0];
    min = arr[0];
    for (int i = 2; i <= n; i++){
        cin >> tmp;
        arr[i] = tmp;
        if (arr[i] > max) {
            max = arr[i];
        }
        else {
            min = arr[i];
        }
    }
    cout << "Max element: " << max << " " << "Min element: " << min << endl;
    return 0;
}