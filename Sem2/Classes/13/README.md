## Вариант 11. Стандартные обобщенные алгоритмы библиотеки STL ##  
| Задача 1 | Задача 2 | Задача 3 |
|----------|----------|----------|
|1. Контейнер - вектор  2. Тип элементов Money | Адаптер контейнера - очередь | Ассоциативный контейнер - словарь |   

| Задание 3 |  Задание 4  | Задание 5 |
|-----------|-------------|-----------|
| Найти среднее арифметическое и добавить его в начало контейнера | Найти элемент с заданным ключом и удалить их из контейнера | Из каждого элемента вычесть минимальный элемент контейнера |

###### Результат программы  
![result13](https://sun9-58.userapi.com/impg/cmNzlqvFpZU5B7uLZqtfazVLiZy5CnBpFKMpwQ/QbNRpKWwCs8.jpg?size=691x546&quality=96&sign=b157106e43d06d50c2207dc71fe1cd95&type=album)  

###### UML-диаграмма
![uml13](https://sun9-28.userapi.com/impg/s-o_KqUELu-ZxRCaVK8y9WKm9oVRVnOMNKVAeQ/LDmPRvX-pks.jpg?size=274x515&quality=96&sign=9937c15f91a44a0e4c743a2aa50f446f&type=album)    

###### Контрольные вопросы  
Кирюшка в костюме  
![kira](https://sun9-38.userapi.com/impg/TaalmjA5gbCgRdyRSujauCPq4b18YpZXA-_IKg/txx6EBKHcow.jpg?size=810x1080&quality=96&sign=b0cc1926432b5ca080ebd928058ed88b&type=album)