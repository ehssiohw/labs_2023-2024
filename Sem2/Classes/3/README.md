## Вариант 11. Перегрузка операций ##  

Создать класс Money для работы с денежными суммами. Число должно быть представлено двумя полями: типа long для рублей, типа int для копеек. Дробная часть числа при выводе на экран должна быть отделена от целой части запятой. Реализовать:  
- операции сравнения;
- добавление копеек (++, постфиксная и префиксная формы) 

###### Результат программы  
![result3](https://sun3-22.userapi.com/impg/WDvbrjiO7Ku6gi8ENKL54j413z2m6YK_0qbk6g/TosVuDb67lg.jpg?size=190x225&quality=96&sign=9338fcab5c22c5d5f1768b9089201e36&type=album)

###### UML-диаграмма  
![uml3](https://sun9-42.userapi.com/impg/0ViukkLTwZdSvALCeU3LXlZmH78-5t-GNpuUvw/TYtvF87P1HI.jpg?size=356x389&quality=96&sign=31821d156c81478ad465b13d0a62d49a&type=album)

###### Контрольные вопросы  

**1.	Для чего используются дружественные функции и классы?**  
Для доступа к скрытым  полям класса извне, то есть расширить интерфейс класса.  

**2.	Сформулировать правила описания и особенности дружественных функций.**  
- объявляется внутри класса, к элементам которого нужен доступ.  
- объявляется с ключевым словом friend.  
- в качестве параметра функции должен передаваться объект или ссылка на объект класса (указатель this не передается).  
- может быть обычной функцией или методом другого раннего класса.  
- на функцию не распространяется действие спецификаторов доступа.  
- место размещения внутри класса - не важно.  
- одна функция может быть дружественной сразу нескольким классам.  
- по возможности необходимо избегать, т.к. нарушают принцип инкапсуляции.  

**3.	Каким образом можно перегрузить унарные операции?**  
- как компонентную функцию класса.  
- как внешнюю (глобальную) функцию.  

**4.	Сколько операндов должна иметь унарная функция-операция, определяемая внутри класса?**  
- операнд один: вызвавший её объект.  

**5.	Сколько операндов должна иметь унарная функция-операция, определяемая вне класса?**  
- один операнд: тип класса.  

**6.	Сколько операндов должна иметь бинарная функция-операция, определяемая внутри класса?**  
- два операнда: вызвавший её объект, нестатический метод с параметрами.  

**7.	Сколько операндов должна иметь бинарная функция-операция, определяемая вне класса?**  
- два операнда: два типа класса.  

**8.	Чем отличается перегрузка префиксных и постфиксных унарных операций?**  
Отличие в семантике.  
++s - сначала выполняется действие ++, затем вся остальная часть программы и возвращается измененное значение s.  
s++ - сначала выполняется программа, возвращает значение s, затем ++.  

**9.	Каким образом можно перегрузить операцию присваивания?**  
Операция присваивания в любом классе - по умолчанию. Если класс содержит поля, память под которые выделяется динамически, необходимо определить собственную операцию присваивания, которая должна возвращать ссылку на объект(!) и принимать в качестве параметра единственный аргумент: ссылку на присваиваемый объект(!).  

**10.	Что должна возвращать операция присваивания?**  
ссылку на присваиваемый объект.  

**11.	Каким образом можно перегрузить операции ввода-вывода?**  
operator>> и operator<< всегда реализуются как внешние дружественные функции, т.к. левым операндом являются потоки (получают ссылку, >> ввод, << вывод). 
 
**12.	В программе описан класс и определен объект этого класса. Выполняется операция ++s. Каким образом, компилятор будет воспринимать вызов функции-операции?**  
```
class Student {
...
    Student& operator++();
...
};
и определен объект этого класса 
Student s;
```
Когда компилятор встречает операцию ++s, он воспринимает это как вызов функции-оператора operator++ для объекта s. В данном случае, поскольку operator++ перегружен как префиксный унарный оператор, возвращаемым значением будет Student& (ссылка на объект).  

**13.	В программе описан класс и определен объект этого класса. Выполняется операция ++s. Каким образом, компилятор будет воспринимать вызов функции-операции?**  
```
class Student {
...
friend Student& operator ++( Student&);
...
};
и определен объект этого класса 
Student s;
```
Вызов ++s будет принят компилятором как вызов этой дружественной функции, которая перегружает оператор инкремента для класса Student.  Поскольку дружественные функции имеют доступ к закрытым членам класса, эта функция-оператор будет иметь прямой доступ к приватным членам объекта s для выполнения соответствующей операции увеличения.  

**14.	В программе описан класс и определены объекты этого класса. Выполняется операция cout<<a<b; Каким образом, компилятор будет воспринимать вызов функции-операции?**  
```
class Student {
...
bool operator<(Student &P);
...
};
и определены объекты этого класса 
Student a,b;
```
Когда компилятор встречает операцию cout << a < b; он воспринимает это как вызов оператора сравнения < для объектов a и b. Поскольку оператор < перегружен в классе Student, компилятор будет использовать эту перегруженную функцию-оператора для выполнения операции сравнения между объектами a и b.  

**15.	В программе описан класс и определены объекты этого класса. cout<<a>b; Каким образом, компилятор будет воспринимать вызов функции-операции?**  
```
class Student {
...
friend bool operator >(const Person&, Person&)
...
};
и определены объекты этого класса 
Student a,b;
```
Компилятор поймет выражение cout << a > b; как вызов оператора > для объектов a и b. Поскольку оператор > не является членом класса Student, а определен как дружественная функция. 

