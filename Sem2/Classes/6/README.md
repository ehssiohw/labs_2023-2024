###### Вариант 11. АТД. Контейнеры
Класс-контейнер СПИСОК с ключевыми значениями тиа int.  
Реализовать операции:  
[] - доступ по индексу;  
int() - определение размера списка;  
+вектор - сложение элементов списка a[i]+b[i];  
-n - переход влево к элементу с номером n (с помощью класса-итератора).

###### Результат программы  
![result6](https://sun9-63.userapi.com/impg/A1WuvnPfREZoMgUr8Dsrfq03IpendEto29oung/9LUG3L3b3RI.jpg?size=211x146&quality=96&sign=ccdc4c7ddaee8f51c598d45d3e1c1143&type=album)

###### UML-диаграмма  
![uml6](https://sun9-21.userapi.com/impg/5wl7_g-EAwvw7iHFjRGGW-83wZTX5rbEBPcZOw/SnzjI_wGtHI.jpg?size=248x596&quality=96&sign=0573c6d52f45647ea502edac6087e33b&type=album)

###### Контрольные вопросы  
**1. Что такое абстратный тип данных? Привести пример.**  
Абстрактный тип данных (АТД) - тип данных, определяемый только через операции, которые могут выполняться над соответствующими объектами безотносительно к способу представления этих объектов.  
Абстрактный тип данных: класс, структура, список...  

**2. Привести примеры абстракции через параметризацию.**  
Абстракция через параметризацию (АП) осуществляется так же, как для функций - использование параметров там, где это имеет смысл. То есть выделяются формальные параметры с возможностью их заменить на фактические в различных контекстах. Выделение формальных параметров позволяет абстрагироваться от конкретного приложение и базируется на общности определенных свойств конкретных приложений.  
`List (int s = 0);`  

**3. Привести примеры абстракции через спецификацию.**  
Абстракция через спецификацию (АС) достигается за счет того, что операции представляются как часть типа. То есть АС позволяет абстрагироваться от внутренней структуры до уровня знания свойств внешних проявлений (результата). Внешние свойства компонента указывают путем описания внешних связей, требований и эффектов (от деталей к поведению, от которого зависят пользователи).  
`class Iterator {
    ...
};
class List {
    ...
};`  

**4. Что такое контейнер? Привести примеры.**  
Контейнер - наиболее общая концепция объединения данных в групу. Набор однотипных элементов.   
Контейнер -- это объект, а имя контейнера -- имя переменной.   
Пример: массив, список, стек, очередь, двусторонняя очередь.  

**5. Какие виды доступа к элементам контейнера существуют? Привести примеры.**  
1. _Последовательный_ - перемещение от элемента к элементу контейнера.  
Например, в массиве последовательный доступ осуществляется с помощью указателя. Объект, который перебирает элементы контейнера -- итератор.  
2. _Прямой_ - доступ по индексу.  
```
a[10];  
```
3. _Ассоциативный_ - доступ по индексу, но индекс уже не номер элемента, а его содержимое.  
```
a["word"];  
```

**6. Какие группы операций выполняют в контейнерах?**  
- поиск элемента по индексу;  
- поиск элемента по содержимому;  
- перемещение от элемента к элементу при помощи указателя:  
    - перейти к первому элементу v.first();  
    - перейти к последнему элементу v.last();  
    - перейти к следующему элементу v.next();  
    - перейти к предыдущему элементу v.prev();   
    - перейти на n-элементов назад v.skip(-n);   
    - перейти на n-элементов вперед v.skip(n);  
    - получить текущий элемент v.cur();  

**7. Что такое итератор?**  
Итератор - объект, с помощью которого осуществляется последовательный доступ к элементам контейнера. 
Может быть реализован как часть класса-контейнера, в виде набора методов (см. выше).  

**8. Каким образом можно организовать объединение контейнеров?** 
1. Простое сцепление: в новый контейнер попадают сначала элементы первого контейнера, потом второго. Операция не коммутативна.  
2. Объединение упорядоченных контейнеров: новый контейнер тоже будет упорядочен. Операция коммутативна.  
3. Объединение контейнеров как множеств: в новый контейнер попадают только те элементы, которые есть хотя бы в одном контейнере. Операция коммутативна.  
4. Объединение контенеров как пересечение множеств: в новый контейнер попадают только те элементы, которые уже есть в обоих контейнерах. Операция коммутативна.  
5. Для контейнеров-множеств реализуется операция вычитания: в контейнер попадают только те элементы первого контейнера, которых нет во вторром. Операция не коммутативна.  
6. Извлечение части элементов из контейнера и создание новго контейнера: операция может быть выполнена с помощью конструктора, а часть контейнера задается двумя итераторами.  

**9. Каким образом может быть реализован контейнер?**  
- подключить нужную библиотеку (например vector, list)  
- реализовать в отдельном заголовочном файле и подключить к основному файлу  

**10. Какой доступ к элементам представляет контейнер, состоящий из элементов "ключ-значение"?**  
Ассоциативный: доступ осуществляется по индексу, но индекс - не номер элемента, а содержимое. Поле с содержимым - ключ, элемент, ассоциирующийся с ключом - значение.  

**11. Как называется контейнер, в котором вставка и удаление элементов выполняется на одном конце контейнера?**  
Неупорядоченный контейнер - стек или очередь.  

**12. Какой из объектов (a, b, c, d) является контейнером?**
> a. int mas = 10;  
> b. int mas;  
> c. struct {char name[30]; int age;} mas;  
> d. int mas[100];  

Ответ: d    

**13. Какой из объектов (a, b, c, d) не является контейнером?**  
> a. int a[] = {1, 2, 3, 4, 5};  
> b. int mas[30];  
> c. struct {char name[30]; int age;} mas[30];  
> d. int mas;  

Ответ: d  

**14. Контейнер реализован как динамический массив, в нем определена операция доступа по индексу. Каким будет доступ элементам контейнера?**   
Прямой, доступ осуществляется напрямую по индексу.  
 
**15. Контейнер реализован как линейный список. Каким будет доступ к элементам контейнера?**  
Последовательный, т.к. реализован как массив и можно осуществлять доступ по элементам с помощью указателя (итератор). 