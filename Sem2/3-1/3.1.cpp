//Перевернуть массив
#include <iostream>
using namespace std;
int main () {
    int n = 9;
    int arr[n] = {1,2,3,4,5,6,7,8,9};
    int temp;
    for(int i = 0; i < n; i++){
        cout << arr[i] << " ";
    }
    cout << endl;
    for(int i = 0; i < n/2; i++){
        temp = arr[i];
        arr[i] = arr[n-i-1];
        arr[n-i-1] = temp;
    }
    for(int i = 0; i < n; i++){
        cout << arr[i] << " ";
    }
    return 0;
}