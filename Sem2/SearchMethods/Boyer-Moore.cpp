#include <iostream>
#include <string>
#include <cstring>
using namespace std;

const int CHAR_NUM = 256;

void calcTable(char* str, int size, int charTable[CHAR_NUM]) {
    for (int i = 0; i < CHAR_NUM; i++) {
        charTable[i] = -1;
    }
    for (int i = 0; i < size; i++) {
        charTable[str[i]] = i;
    }
}

void BoyerMoore(char* str, char* pat) {
    int patSize = strlen(pat);
    int strSize = strlen(str);
    
    int shiftChar[CHAR_NUM];

    calcTable (pat, patSize, shiftChar);
    bool isFound = false;
    int shift = 0;

    while (shift <= (strSize - patSize)) {
        int endPat = patSize - 1;

        while (endPat >= 0 && pat[endPat] == str[shift + endPat]) {
            endPat--;
        }

        if (endPat < 0) {
            cout << "pattern found at index: " << shift << endl;
                shift += (shift + patSize < strSize) ? patSize - shiftChar[str[shift+patSize]] : 1;
                isFound = true;
       
        }
        else {
            shift += max(1, endPat - shiftChar[str[shift+endPat]]);
        }
    if (!isFound) {
        cout << "not found" << endl;
        }       
    }
}

int main () {
    char str[100];
    char pat[100];
    cout << "Enter a string: ";
    cin >> str;

    cout << "Enter a pattern: ";
    cin >> pat;

    cout << "Boyer-Moore search result: ";
    BoyerMoore(str, pat);
    return 0;
}

