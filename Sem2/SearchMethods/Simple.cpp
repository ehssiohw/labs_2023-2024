#include <iostream>
#include <string>
using namespace std;

int main () {
string str, pat;

cout << "Enter a string: ";
cin >> str;

cout << "Enter a pattern: ";
cin >> pat;

cout << "Simple (linear) search result (index): " << str.find(pat) << endl;
return 0;
}