#include <iostream>
#include <ctime>

using namespace std;

void countSort (int arr[], int length) {
    int* countArr = new int[length];
    int max = arr[0];

    for (int i = 1; i < length; i++){
        if (arr[i] > max) { max = arr[i];}
    }
    int* count = new int[max+1];
    
    for (int i = 0; i <= max; ++i) {
        count[i] = 0;
        }

    for (int i = 0; i < length; i++){
        count[arr[i]]++;
        }

    for (int i = 1; i <= max; i++){
        count[i] += count[i - 1];
    }
    for (int i = length - 1; i >= 0; i--){
        countArr[count[arr[i]] - 1] = arr[i];
        count[arr[i]]--;
    }
    for (int i = 0; i < length; i++){
        arr[i] = countArr[i]; 
    }
    delete[] count;
    delete[] countArr;

    for (int i = 0; i < length; i++){cout << arr[i] << " ";}
}

int main () {
    srand(time(0));
    int str[100];
    int pat = 0, sizeArr = 0;
    bool f = false; 


    cout << "Enter size: ";
    cin >> sizeArr;
    if (sizeArr >= 2) { 
        for (int i = 0; i < sizeArr; i++) {
        str[i] = rand () % 100;
    }
    cout << "Array" << endl;
    for (int i = 0; i < sizeArr; i++) {
        cout << str[i] << " ";
    }
    cout << endl;

    cout << "Sorting array" << endl;
    countSort(str, sizeArr);
    cout << endl;

    cout << "Enter a number to find: ";
    cin >> pat;
    
    int leftPartIndex = 0;
    int rightPartIndex = sizeArr - 1;
    int middlePartIndex;

    while ((leftPartIndex <= rightPartIndex) && (!f)) {
        middlePartIndex = (leftPartIndex + rightPartIndex) / 2;

        if (str[middlePartIndex] == pat) {
            f = true;
            rightPartIndex = middlePartIndex - 1;
        }
        else {
            leftPartIndex = middlePartIndex + 1;
        }
    }

    if (f) {
        cout << "Pattern " << pat << " index: " << middlePartIndex;
    }
    else {
        cout << "Pattern doesn't found";
    }
    }
    else {
        cout << "Error (size)" << endl;
    }
    return 0;
}