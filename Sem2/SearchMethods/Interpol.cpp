#include <iostream>
#include <ctime>
using namespace std;

void countSort (int arr[], int length) {
    int* countArr = new int[length];
    int max = arr[0];

    for (int i = 1; i < length; i++){
        if (arr[i] > max) { max = arr[i];}
    }
    int* count = new int[max+1];
    
    for (int i = 0; i <= max; ++i) {
        count[i] = 0;
        }

    for (int i = 0; i < length; i++){
        count[arr[i]]++;
        }

    for (int i = 1; i <= max; i++){
        count[i] += count[i - 1];
    }
    for (int i = length - 1; i >= 0; i--){
        countArr[count[arr[i]] - 1] = arr[i];
        count[arr[i]]--;
    }
    for (int i = 0; i < length; i++){
        arr[i] = countArr[i]; 
    }
    delete[] count;
    delete[] countArr;

    for (int i = 0; i < length; i++){cout << arr[i] << " ";}
}

int interpolatingSearch (int str[], int strSize, int pattern) {

int leftPart = 0, rightPart = strSize - 1, mid;
if ((str[leftPart] < pattern) && (str[rightPart] >= pattern)) {  
while ((str[leftPart] < pattern) && (str[rightPart] >= pattern)) {
    mid = (leftPart + ((pattern - str[leftPart])) * (rightPart - leftPart)) / (str[rightPart] - str[leftPart]);
        if (str[mid] < pattern) {
            leftPart = mid + 1;
        }
        else if (str[mid] > pattern) {
            rightPart = mid - 1;
        }
        else {
            return mid;
        }
        if (str[leftPart] == pattern) {
            return leftPart;

        }
        else {
            cout << "not found";
            return -1;
        }
    }
}
    else {
        cout << "Error (pattern)";
    }
}

int main(){
srand(time(0));

int size, pat = 0, result, str[size];

cout << "Enter array size: ";
cin >> size;
if (size >= 2) {
    for (int i = 0; i < size; i++) {
        str[i] = rand() % 100;
    }
    cout << "Array" << endl;
    for (int i = 0; i < size; i++) {
        cout << str[i] << " ";
    }
    cout << "\n";

    cout << "Sorting array" << endl;
    countSort (str, size);
    cout << "\n";

    cout << "Enter pattern: ";
    cin >> pat;

    result = interpolatingSearch(str, size, pat);
    if (result != -1) {
        cout << "Interpolating search result: " << result;
    }
}
else {
    cout << "Error(size)";
}
return 0;
}

