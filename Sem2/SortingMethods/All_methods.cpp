#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>

using namespace std;

void createArray (int arr[], int size) {
    srand(time(0));
    if (size >= 2) {
        for (int i = 0; i < size; i++){
            arr[i] = rand() % 100;
        }    
    }
    else {
        cout << "Error (size)" << endl;
        exit(0);
    }

    for (int i = 0; i < size; i++) {cout << arr[i] << " ";} cout << endl;
}

void countSort (int arr[], int length) {
    int* countArr = new int[length];
    int max = arr[0];

    for (int i = 1; i < length; i++){
        if (arr[i] > max) { max = arr[i];}
    }
    int* count = new int[max+1];
    
    for (int i = 0; i <= max; ++i) {
        count[i] = 0;
        }

    for (int i = 0; i < length; i++){
        count[arr[i]]++;
        }

    for (int i = 1; i <= max; i++){
        count[i] += count[i - 1];
    }
    for (int i = length - 1; i >= 0; i--){
        countArr[count[arr[i]] - 1] = arr[i];
        count[arr[i]]--;
    }
    for (int i = 0; i < length; i++){
        arr[i] = countArr[i]; 
    }
    delete[] count;
    delete[] countArr;

    for (int i = 0; i < length; i++){cout << arr[i] << " ";}
}

void quickSort(int* arr, int first, int last) {
	if (first < last){
		int left = first;
		int right = last;
		int mid = arr[(left + right) / 2];
		while (left < right){
			while (arr[left] < mid) {left++;}
			while (arr[right] > mid) {right--;}
			if (left <= right){
				int tmp = arr[left];
				arr[left] = arr[right];
				arr[right] = tmp;
				left++;
				right--;
			}
		}
		quickSort(arr, first, right);
		quickSort(arr, left, last);
	}
	return;
}

void bucketSort(int arr[], int length) {
	int buckets[10][10];
	int bucketSizes[10] = { 0 };
	for (int i = 0; i < length; i++) {
		int bucketIndex = arr[i] / 10;
		buckets[bucketIndex][bucketSizes[bucketIndex]] = arr[i];
		bucketSizes[bucketIndex]++;
	}

	for (int i = 0; i < 10; i++) {
		for (int j = 1; j < bucketSizes[i]; j++) {
			int temp = buckets[i][j];
			int k = j - 1;
			while (k >= 0 && buckets[i][k] > temp) {
				buckets[i][k + 1] = buckets[i][k];
				k--;
			}
			buckets[i][k + 1] = temp;
		}
	}

	int index = 0;
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < bucketSizes[i]; j++){
			arr[index++] = buckets[i][j];
		}
	}
    for (int i = 0; i < length; i++){cout << arr[i] << " ";}
}

void merge(int arr[], int leftPart, int midPart, int rightPart){ //сливаем
    int leftRange = midPart - leftPart + 1;
    int rightRange = rightPart - midPart;

    int* leftArr = new int[leftRange];
    int* rightArr = new int[rightRange];

    for (int i = 0; i < leftRange; i++) {leftArr[i] = arr[leftPart + i];}
    for (int i = 0; i < rightRange; i++) {rightArr[i] = arr[midPart + 1 + i];}

    int leftIndex = 0;
    int rightIndex = 0;
    int mergeIndex = leftPart;

    while (leftIndex < leftRange && rightIndex < rightRange){
        if (leftArr[leftIndex] <= rightArr[rightIndex]){
            arr[mergeIndex] = leftArr[leftIndex];
            leftIndex++;
        } else {
            arr[mergeIndex] = rightArr[rightIndex];
            rightIndex++;
        }
        mergeIndex++;
    }

    while (leftIndex < leftRange){
        arr[mergeIndex] = leftArr[leftIndex];
        leftIndex++;
        mergeIndex++;
    }
    while (rightIndex < rightRange){
        arr[mergeIndex] = rightArr[rightIndex];
        rightIndex++;
        mergeIndex++;
    }

    delete[] leftArr;
    delete[] rightArr;
}

void mergeSort(int arr[], int start, int end){ //делим
    if (start >= end){return;}
    
    int mid = start + (end - start) / 2;
    mergeSort(arr, start, mid);
    mergeSort(arr, mid + 1, end);
    merge(arr, start, mid, end);
}

void shellSort(int arr[], int lenght) {
	for (int step = lenght / 2; step > 0; step /= 2) {
        for (int i = step; i < lenght; ++i) {
            for (int j = i - step; j >= 0 && arr[j] > arr[j + step]; j -= step) {
                swap(arr[j], arr[j + step]);
            }
        }
	}
    for (int i = 0; i < lenght; i++) {cout << arr[i] << " ";}
}

int quickSort(int arr[], int startElem, int endElem) {
    int pivot = arr[endElem];
    int pivIndex = startElem;
    for (int i = startElem; i < endElem; i++) {
        if (arr[i] <= pivot) {
            swap(arr[i], arr[pivIndex]);
            pivIndex++;
        }
    }
    swap(arr[pivIndex], arr[endElem]);
    return pivIndex;
}

void LomutoSort(int arr[], int start, int end) {
    if (start >= end)  return;
       int pivot = quickSort(arr, start, end);
       LomutoSort(arr, start, pivot - 1);
       LomutoSort(arr, pivot + 1, end);
}

int main () {
    string choise;
    int sizeArray;
    int arr[sizeArray];
    cout << "Do you want to create an array? y/n" << endl;
    cin >> choise;
    if (choise == "y") {
        cout << "Enter a count of elements in array (size): ";
        cin >> sizeArray;
        createArray(arr, sizeArray);
        int choise_switch;

    cout << "Choose the variant: \n" << "1. Counting sort \n" << "2. Quick sort \n" << "3. Bucket sort \n" << "4. Merge sort \n" << "5. Shell sort \n" << "6. Lomuto sort \n";
    cin >> choise_switch;
        if (choise_switch >= 1 && choise_switch <=5) {
    switch (choise_switch) {
        
        //1. Counting sort
        case (1): {
            cout << "Using counting sort: " << endl;
            countSort(arr, sizeArray);
            break;
        }
        
        //2. Quick sort
        case (2): {
            cout << "Using quick sort: \n";
            quickSort(arr, 0, sizeArray-1);
        for (int i = 0; i < sizeArray; i++){cout << arr[i] << " ";}
            break;
        }
        
        //3. Bucket sort
        case (3): {
            bucketSort(arr, sizeArray);
            break;
        }

        //4. Merge sort
        case (4): {
            mergeSort (arr, 0, sizeArray-1);
            for (int i = 0; i < sizeArray; i++){cout << arr[i] << " ";}
            break;
        }
        //5. Shell sort
        case (5): {
            shellSort(arr, sizeArray);
            break;
            }
        case (6): {
            LomutoSort(arr, 0, sizeArray-1);
            for (int i = 0; i < sizeArray; i++){cout << arr[i] << " ";}
            break;
            }
        }
    }
        else {exit(0);}
}
    else { exit(0);}
    return 0;
}

