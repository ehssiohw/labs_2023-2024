Реализация сложных сортировок + менюшка.
Блочная; Хоара, Ломуто (быстрые); Слиянием; Шелла; Подсчетом; Многофазная.

###### Results  
Polyphase  
![pol](https://sun9-39.userapi.com/impg/dRoR9ERD4VRclXXC75ezEf5OSsKKfQs20Z_8OA/KYxYTug_trA.jpg?size=594x86&quality=96&sign=72a764c746e65adf9a0f06d1da4edbc8&type=album)  

All methods  
![all1](https://sun9-24.userapi.com/impg/kfTfIEcBJRn8UsJ8K1UQZLjRQQF_fKFoHG40Bw/QYqKT1LsCe0.jpg?size=419x619&quality=96&sign=68f9ba73bfc438e1eb7943da4e103deb&type=album)  
![all2](https://sun9-62.userapi.com/impg/htcLGXhXkCIA9LpR82-rkHamQBTIqaOIRe39Cw/2dPiZc7q8yo.jpg?size=430x573&quality=96&sign=6268e4afbccdb4a6a5cc22f7820e6d28&type=album)  
![all3](https://sun9-14.userapi.com/impg/q2mvG26Yx0EoMDE2--acZrB0srEjSgu2bWhBbQ/OVHbSO-fLlk.jpg?size=408x576&quality=96&sign=c61e8f9f8d7908c91409e58f398f6c9e&type=album)  
![all4](https://sun9-64.userapi.com/impg/oV321WzE2M3EnvZ2F17g9pmfDSOQcynhFK0X7Q/_3V_7NafHu8.jpg?size=434x94&quality=96&sign=f12e24b1e3a13fa934dbaecfa1dfb091&type=album)  