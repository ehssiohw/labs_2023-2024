## Лабораторная работа по графам ##  

:blush: _**Как установить: скачать папку src, в Qt запустить .pro**_ :blush:  
Вариант:
![var](https://sun9-59.userapi.com/impg/9ASNrDLumIRthB6DJ_xY8-dUuLJDiFXDLsZGow/s7DE5oDODyc.jpg?size=715x436&quality=96&sign=cd20edbd2c88b910ee8a75ea40ecf5e0&type=album)  

### Анализ задачи  
подробнее см. в коде  

1. Отдельно создаем класс ребра (edge), класс вершины (vertex) и граф (graph).
2. Класс Edge отвечает за рисование ребра, хранение информации о ребре, управление его весом и цветом. Он также вычисляет положение, форму и размеры ребра на графической сцене.
3. Класс Vertex представляет вершину в графическом объекте на сцене и содержит логику для управления вершинами в графе. В нем реализованы основные функции для работы с вершинами, такие как установка и изменение цвета, управление именем вершины, добавление, удаление и обновление рёбер, расчет сил для избежания столкновений, обработка изменений позиции вершины, а также методы отображения и взаимодействия с вершиной на сцене.  
3. Класс Graph объединяет в себе реализацию Vertex + Edge.
Создает общую графическую оболочку. Задает цвета фона, предметов. Тут же реализован алгоритм Дейкстры, обход в ширину и глубину + задача Коммивояжера. Реализована матрица смежности.  

### Демонстрация работы программы  

**Добавление вершины**  
![addvertex](https://vk.com/doc440424702_676442469?hash=p77iZFbVpIyjovZuJSCkPVtczNdpg6oPg7SQeVNyOzc&dl=v1dfdTupPQz048I8oMwP1hwi9ZuyqK7bNh5hdF6M06z&wnd=1&module=im)

**Добавление ребер**  
![addedge](https://vk.com/doc440424702_676442516?hash=wTw0DH4ciuusOAzY3anDAJg7ZnFw6zhGXIMiC86NR1z&dl=l4TOqoHKKagHLO1SpDsK9dZkkNras8UHZhNUpwd8MqX&wnd=1&module=im)  

**Удаление вершины и удаление ребра**  
![del](https://vk.com/doc440424702_676442574?hash=Pif6HFgJmfSyVkirux2ZMyVzzA7DWEHlw9E1z5QFEmD&dl=GNdRbvZNCTXOszTNJ9K6lxUquLryzHamgHZRL2r6ECw&wnd=1&module=im)  

**Изменение веса**  
![updw](https://vk.com/doc440424702_676442943?hash=hXto1xgfiF7jEQoZWgqdqqmUHl8rCbMbg1UQz9ZEzZP&dl=nB2oUiMowKcCRf2RkKzYkGp4AL9Z664QkSybxl56ppz&wnd=1&module=im)  

**Обход в глубину**  
![dfs](https://vk.com/doc440424702_676442670?hash=6xyCBBCuyZ2Z6w7xgMp3zE8VPrsGBuItZ0kOIm7bnWs&dl=DplVZF3ljQqdxx1SZRQxHOxZrhBft6Awe0kgNOiUTf0&wnd=1&module=im)  

**Обход в ширину**  
![bfs](https://vk.com/doc440424702_676442690?hash=MwoRzExkJUn99GRKWwHx85RxZpzbfqtwv1ie6eWT6kD&dl=yiX2kpaceyhgWHrKHwenR0xNpVpZ5WD5njqUMvuIALs&wnd=1&module=im)  

**Алгоритм Дейкстры и матрица смежности**  
![dej](https://vk.com/doc440424702_676442733?hash=jHTtPo2WF7pNrgydFSTNtWoQvOYQELR2p4rLelPFYPL&dl=FVPzQUkoVSfdymkf1vPq8I7Vi4h91eGJy6RLz32W8hc&wnd=1&module=im)  

**Удалить все**  
![delall](https://vk.com/doc440424702_676442944?hash=icfq28cPVLfVW7Auj5zVnrteEZxxHL9mxrZSd4F2Zuk&dl=AO7FWmXlrF8UO8t61hE29xapkEHfO0BzYe4uU9jJ2H8&wnd=1&module=im)  

### UML-диаграмма  
![uml](https://psv4.userapi.com/c237331/u440424702/docs/d22/8b415164e52f/1.png?extra=dOw1o18uA3w-dHQoZ7aF0BY9RdCqrEJAsticQyF6G6C-wUp2IHdYG29L6gpJ9CaXmDKcaT_kUaDqNCXpOOCt-HTFr-nDrXvx_SfUhl_hEH9NbBr5Hok2FihWEReXwIfoTonMA01OMnc1dTDMGpzC_q3QCg)