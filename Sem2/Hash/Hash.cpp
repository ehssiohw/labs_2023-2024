﻿#include<iostream>
#include<string>

using namespace std;

string name[30] = { "Софья", "Кира", "Анна", "Вероника", "Ева", "Мирослава", "Виктория", "Анастасия", "Полина", "Владислава", "Алиса", "Милана", "Варвара", "Дарья", "Василиса", "Екатерина", "Александра", "Ксения", "Арина", "Елизавета", "Валерия", "Есения", "Елена", "Ольга", "Наталья", "Юлия", "Надежда", "Ирина", "Татьяна", "Мария" };
string patronymic[30] = { "Ивановна", "Андреевна", "Антоновна", "Сергеевна", "Иосифовна", "Георгиевна", "Борисовна", "Владиславовна", "Дмитриевна", "Никитична","Даниловна", "Артемовна", "Александровна", "Валерьевна", "Ильинична", "Григорьевна", "Артуровна", "Денисовна", "Алексеевна", "Макаровна", "Марковна", "Кирилловна", "Михайловна", "Константиновна", "Николаевна", "Германовна", "Иннокентьевна", "Богдановна", "Станиславовна", "Васильевна" };
string surname[30] = { "Карпова", "Афанасьева", "Власова","Маслова","Исакова","Тихонова","Аксёнова","Гаврилова","Родионова","Котова","Горбунова","Кудряшова","Быкова","Зуева","Третьякова","Савельева","Панова","Рыбакова","Суворова","Абрамова","Воронова","Мухина","Архипова","Трофимова","Мартынова","Емельянова","Горшкова","Чернова","Овчинникова","Селезнёва" };
string ID[30] = { "5717","5321","2323","8321","3232","9999","9210","0105","6521","8891","1118","5617","9596","3490","9085","7254","7152","2015","8047","0001","3281","2131","8328","3727","2814","9348","2388","4211","3284","5521" };
string dates[30] = { "12.12.2011","18.05.1030","09.01.2000","21.03.2003","19.10.2010","23.09.2009","24.10.2010","25.11.2011","26.12.2012","27.01.1988","28.02.1967","29.04.1999","30.05.1867","31.12.1999","01.06.1976","02.07.1980","03.08.1982","04.09.1955","05.10.1948","06.11.2014","16.05.1977","29.06.2004","19.02.2001","09.11.1996","27.03.2002","30.10.2000","27.08.2022","28.02.2019","29.10.1932","30.09.2014" };
int countCollision = 0;

struct Person {
	Person() { name = "NULL"; passport = "NULL"; birthday = "NULL"; }
	string name, passport, birthday;
};

struct hashTable {
	Person* arr;
	hashTable(int size) { arr = new Person[size]; }
	~hashTable() { delete[]arr; }
	void index(string passport, const int size);
	void add(Person tmp, const int size);
};

void showTable(hashTable* table, const int size);
void show(const Person tmp);

Person creator();

string get_name();
string get_birthday();
string get_passport();

int get_number();
int get_string(string str);

void printPerson(const Person* const arr, const int size);
int hashFunc(string str, const int size);
void create(Person* arr, const int countCollision);


void hashTable::index(string passport, const int size) {
	int hash = hashFunc(passport, size);
	int index = hash;
	while (arr[index].passport != passport && index < size) { index++; }
	if (index >= size) {
		index = 0;
		while (arr[index].passport != passport && index < hash) { index++; }
		if (index >= hash) { cout << "Person with passport " << passport << " not found" << endl; }
		else { cout << "Person with passport " << passport << " found. Index " << index << endl; }
	}
	else { cout << "Person with passport " << passport << " found. Index " << index << endl; }
}


void hashTable::add(Person tmp, const int size) {
	int index = hashFunc(tmp.passport, size);
	int hash = index;
	if (arr[index].name == "NULL") { arr[index] = tmp; return; }
	else {
		while (index < size) {
			if (arr[index].name == "NULL") { arr[index] = tmp; return; }
			index++;
			countCollision++;
		}
		if (index >= size) {
			index = 0;
			countCollision++;
			while (index < hash) {
				if (arr[index].name == "NULL") {
					arr[index] = tmp;
					return;
				}
				index++;
				countCollision++;
			}
		}
	}
}

Person creator() {
	Person tmp;
	tmp.name = get_name();
	tmp.birthday = get_birthday();
	tmp.passport = get_passport();
	return tmp;
}

string get_name() { return(surname[get_number()] + " " + name[get_number()] + " " + patronymic[get_number()]); }
string get_birthday() { return dates[get_number()]; }
string get_passport() { return ID[get_number()]; }

void show(const Person tmp) {
	cout << "NAME: " << tmp.name << endl;
	cout << "BIRTH: " << tmp.birthday << endl;
	cout << "PASS: " << tmp.passport << endl;
	cout << "\n";
}

void printPerson(const Person* const arr, const int size) {
	for (int i = 0; i < size; i++) { show(arr[i]); }
}

void create(Person* arr, const int countCollision) {
	for (int i = 0; i < countCollision; i++) { arr[i] = creator(); }
}

int get_number() { return rand() % 30; }

int get_string(string str) {
	string yam = ID[11];
	int sum = stoi(yam);
	return sum;
}

int hashFunc(string str, const int size) {
	double a = 0.618 * get_string(str);
	double c = size * (a - static_cast<int>(a));
	return static_cast<int>(c);
}

void showTable(hashTable* table, const int size) {
	for (int i = 0; i < size; i++) { show(table->arr[i]); }
}

int main() {
	srand(time(0));
	setlocale(LC_ALL, "Rus");
	int sizeArr;
	cout << "Enter array size: "; cin >> sizeArr; cout << "\n";

	Person* arr = new Person[sizeArr];
	hashTable table(sizeArr);
	create(arr, sizeArr);
	for (int i = 0; i < sizeArr; i++) { table.add(arr[i], sizeArr); }

	showTable(&table, sizeArr);
	table.index("5617", sizeArr);

	showTable(&table, sizeArr);

	cout << "Collisions (size " << sizeArr << ") = " << countCollision << endl;
	delete[] arr;
	return 0;
}