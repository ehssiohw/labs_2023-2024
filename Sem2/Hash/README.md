## Вариант 20. Поиск данных с помощью хэш-таблиц ##

Данные: ФИО, дата рождения, номер паспорта.  
Ключ (string): дата рождения.  
Хэш-функция:  

$H(k) = [M(kAmod1)]$

_0 < A < 1_   
_mod1_ - получение дробной части   
_[]_ - получение целой части  

Метод рехеширования: метод цепочек.  

###### Результат программы 
![reult1](https://sun9-54.userapi.com/impg/6FHTkWuilDqWvtEf9xVEE_AkHxzUEpW0W4dwFw/jXdT90FVvhE.jpg?size=348x555&quality=96&sign=3e4cedfa6f146c52ffdb200225caa83a&type=album)
![result2](https://sun9-15.userapi.com/impg/VN-vilAkXnOdyuEsW8NB7NT785Q7aggyguFGaQ/UQ3FwurMMzg.jpg?size=344x415&quality=96&sign=b2c1c1bc6b70f5651f7342d4d9fcf092&type=album)

###### Flowchart
![flowchart](https://psv4.userapi.com/c909228/u440424702/docs/d50/8b922efea4d7/Hash.jpg?extra=YaHh6MItG2aAJiOV-RScZL7eCoU3B1qUdQ082TzbKJF4rF06al1CgQs7MXEtopEPDW_nSlzBZHV6QKO4FnlMRMw6l52nuyjUZ7zPEQMjwcFHh29DwmKBWD7KbT-T-ne0_BuaKw6mFkY5c4iAbji95KE)
