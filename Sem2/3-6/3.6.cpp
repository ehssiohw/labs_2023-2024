//Найти максимальный элемент массива и количество с ним совпадающих чисел
#include <iostream>
using namespace std;
int main () {
    int max, tmp, n=5, k=0;
    int arr[n];
    cin >> tmp;
    max = tmp;
    arr[0] = tmp;
    for (int i = 2; i <= n; i++){
        cin >> tmp;
        arr[i] = tmp;
        if (arr[i] > max) {
            max = arr[i];
        }
    }
    for (int i = 1; i <= n; i++) {
        if (arr[i] == max) {
            k++;
        }
    }
    cout << "Max element: " << max << " " << "Count: " << k << endl;
    return 0;
}