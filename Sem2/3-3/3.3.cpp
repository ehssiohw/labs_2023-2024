//Сдвинуть массив циклически на k элементов влево
#include <iostream>
using namespace std;
int main () {
    const int n = 10; 
    int k = 4, tmp;
    int arr[n] = {1,2,3,4,5,6,7,8,9,10};
    for(int i = 0; i < n; i++){
        cout << arr[i] << " ";
    }
    cout << endl;

    for (int i = 0; i < k; i++) { // у нас 3 итерации
      tmp = arr[n-1]; //присваиваем tmp последний элемент 
      for (int j = n-1; j > 0; j--) { // без последнего элемента цикл
        arr[j] = arr[j-1]; //сдвигаем вправо
      }
      arr[0] = tmp; //последний теперь первый
      cout << endl;
    for (int j = 0; j < n; j++) {
        cout << arr[j] << " ";
    }
    }
    return 0;
}