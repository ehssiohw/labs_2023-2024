//Проверить, упорядочен ли массив целых чисел по возрастанию.
//Задан одномерный массив целых чисел. Проверить, упорядочен ли он по возрастанию.
#include <iostream>
using namespace std;
int main () {
    int n = 5, tmp;
    int arr[n];
    bool f = true;
    cin >> tmp;
    arr[0] = tmp;
    for (int i = 2; i <= n; i++){
        cin >> tmp;
        arr[i] = tmp;
        if (arr[0] < tmp && f) {
            f = true;
        }
        else {
            f = false;
        }
    }
    if (f) {
        cout << "Ascending";
    }
    else {
        cout << "Descending";
    }
    return 0;
}