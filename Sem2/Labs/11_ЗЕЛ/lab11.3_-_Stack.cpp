#include <iostream>
#include <string>
using namespace std;

struct node {
    int data;
    node* next;
};

struct Stack {
    node* top = nullptr;
    
    void push(int Data){
        node* newNode = new node;
        newNode->data = Data;
        newNode->next = top;
        top = newNode;
    }

    int pop(){
        if (top == nullptr) {
            cout << "Stack is empty" << endl;
        }
        else {
            int element = top->data;
            node* temp = top;
            top = top->next;
            delete temp;
            return element;
        }
    }
    
    void print() {
    node* newNode = top;
    while (newNode != nullptr) {
        cout << newNode->data << " ";
        newNode = newNode->next;
    }
    cout << endl;
}

void del(int Data) {
    node* newNode = top;
    node* prev = nullptr;

    while (newNode != nullptr) {
        if (newNode->data == Data) {
            if (prev == nullptr) {
                top == newNode->next;
                delete newNode;
                return;
            }
            else {
                prev->next = newNode->next;
                delete newNode;
                return;
            }
        }
        prev = newNode;
        newNode = newNode->next;
    }
    cout << "Element doesn't exist" << endl;
}
};

int main () {
    Stack stack;
    int count, element;
    string choice;

    cout << "Enter the number of elements: ";
    cin >> count;
    
    if (count >= 0) {
    for (int i = 0; i < count; i++) {
        cout << "Enter the " << i + 1 << " element: ";
        cin >> element;
        stack.push(element);
    }
    stack.print();

        cout << "Do you want to delete something? y/n ";
        cin >> choice;
        if (choice == "y") {
            cout << "Enter the element to delete: ";
            cin >> element;
            stack.del(element);
        }
        else if (choice == "n") {
            cout << "Ok" << endl;
        }
        else {
            cout << "Error (choice)" << endl;
        }
        stack.print();
    
        cout << "Do you want to add something? y/n ";
        cin >> choice;
        if (choice == "y") {
            cout << "Enter the element to add: ";
            cin >> element;
            stack.push(element);
        }
        else if (choice == "n") {
            cout << "Ok" << endl;
        }
        else {
            cout << "Error (choice)" << endl;
        }
        stack.print();
    }
    else {
        cout << "Error (count)";
    }
    return 0;
}