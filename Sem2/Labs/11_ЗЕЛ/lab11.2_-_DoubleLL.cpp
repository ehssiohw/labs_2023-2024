#include <iostream>
#include <string>
using namespace std;

struct node
{
    int data;
    node* next;
    node* prev;
};

struct DoubleList {
    node* head = nullptr;
    node* tail = nullptr;

    void push_back (int Data) {
        node* newNode = new node;
        newNode->data = Data;
        newNode->next = head;
        newNode->prev = nullptr;

        if (head != nullptr) {
            head->prev = newNode;
        }
        head = newNode;
        if (tail == nullptr) {
            tail = newNode;
        }
    }

    void print() {
        node* newNode = head;
        while (newNode) {
            cout << newNode->data << " ";
            newNode = newNode->next;
        }
        cout << endl;
    }

    void del(int Data) {
    node* newNode = head;
    while (newNode != nullptr) {
        if (newNode->data == Data) {
            if (newNode == head) {
                head = newNode->next;
                if (head != nullptr) {
                    head->prev = nullptr;
                } else {
                    tail = nullptr;
                }
                delete newNode;
                return;
            } else if (newNode == tail) {
                tail = newNode->prev;
                tail->next = nullptr;
                delete newNode;
                return;
            } else {
                newNode->prev->next = newNode->next;
                newNode->next->prev = newNode->prev;
                delete newNode;
                return;
            }
        }
        newNode = newNode->next;
    }
    cout << "Element not found" << endl;
}
};

int main () {
    DoubleList list;
    int element, count;
    string choice;

    cout << "Enter the number of elements: ";
    cin >> count;
    
    if (count >= 0) {
        for (int i = 0; i < count; i++) {
            cout << "Enter the " << i + 1 << " element: ";
            cin >> element;
            list.push_back(element);
        }
        list.print();

        cout << "Do you want to delete something? y/n ";
        cin >> choice;
        if (choice == "y") {
            cout << "Enter the element to delete: ";
            cin >> element;
            list.del(element);
        }
        else if (choice == "n") {
            cout << "Ok" << endl;
        }
        else {
            cout << "Error (choice)" << endl;
        }
        list.print();
    
        cout << "Do you want to add something? y/n ";
        cin >> choice;
        if (choice == "y") {
            cout << "Enter the element to add: ";
            cin >> element;
            list.push_back(element);
        }
        else if (choice == "n") {
            cout << "Ok" << endl;
        }
        else {
            cout << "Error (choice)" << endl;
        }
        list.print();
    }
    else {
        cout << "Error (count)";
    }
    return 0;
}

