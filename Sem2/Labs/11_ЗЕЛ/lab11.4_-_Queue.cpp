#include <iostream>
#include <string>
using namespace std;

struct node {
    int data;
    node* next;
};

struct Queue {
    node* front = nullptr;
    node* back = nullptr;    
    
    void push(int Data){
        node* newNode = new node;
        newNode->data = Data;
        newNode->next = top;
        if (front == nullptr) {
            front = newNode;
        } else {
            back->next = newNode;
        }
        back = newNode;
    }

    int pop(){
        node* cur = front;
        node* prev = nullptr;

        while (cur != nullptr) {
            if (cur->data == data) {
                if (prev == nullptr) {
                    front = cur->next;
                    delete cur;
                    return;
                }
                else {
                    prev->next = cur->next;
                    delete cur;
                    return;
                }
            }
            prev = cur;
            cur = cur->next;
        }
        cout << "Queue is empty" << endl;
    }
    
    void print() {
    node* newNode = front;
    while (newNode != nullptr) {
        cout << newNode->data << " ";
        newNode = newNode->next;
    }
    cout << endl;
    }
};

int main () {
    Queue queue;
    int count, element;
    string choice;

    cout << "Enter the number of elements: ";
    cin >> count;
    
    if (count >= 0) {
    for (int i = 0; i < count; i++) {
        cout << "Enter the " << i + 1 << " element: ";
        cin >> element;
        queue.push(element);
    }
    queue.print();

        cout << "Do you want to delete something? y/n ";
        cin >> choice;
        if (choice == "y") {
            cout << "Enter the element to delete: ";
            cin >> element;
            queue.pop(element);
        }
        else if (choice == "n") {
            cout << "Ok" << endl;
        }
        else {
            cout << "Error (choice)" << endl;
        }
        queue.print();
    
        cout << "Do you want to add something? y/n ";
        cin >> choice;
        if (choice == "y") {
            cout << "Enter the element to add: ";
            cin >> element;
            queue.push(element);
        }
        else if (choice == "n") {
            cout << "Ok" << endl;
        }
        else {
            cout << "Error (choice)" << endl;
        }
        queue.print();
    }
    else {
        cout << "Error (count)";
    }
    return 0;
}