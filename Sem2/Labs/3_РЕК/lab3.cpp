#include <iostream>
#include <cmath>
using namespace std;

int board[8][8];
const int queen = -1;

int fact(int n) { //факториал
    if (n == 0){
        return 1;
    }
    return n * fact(n-1);
}

float sum(int n, int x){ //сумма элементов ряда
        float s = 0;
    if (n < 2) {
        return 1;
    }
    return s += ((pow(log(3), n)*pow(x, n))/fact(n));
}

int fib(int n){ //числа фибоначчи
    if (n<=2) {
        return 1;
        }
    else {
        return fib(n-1)+fib(n-2);
        }
}

void towerOfHanoi(int n, char from, char to, char tmp){ 
    if (n == 0) {
        return;
    }
    towerOfHanoi(n - 1, from, tmp, to);
    cout << "Move ring " << n << " from " << from << " to " << to << endl;
    towerOfHanoi(n - 1, tmp, to, from);
} 

////////////////////////////////
void resetBoard(){
    for (int i = 0; i < 8; i++){
        for (int j = 0; j < 8; j++){
            board[i][j] = 0;
        }
    }        
}

void showBoard() {
    for (int i = 0; i < 8; i++){
        for (int j = 0; j < 8; j++){
            if (board[i][j] == queen){
                cout << "Q ";
            }
            else {
                cout << "[ ] ";
            }
        }
        cout << endl;
    }
}

void setQueen (int x, int j) {
    int d; //диагональ
    for (int i = 0; i < 8; ++i) { //сначала увеличим, потом делаем тело
    board[i][j]++; //указываем на клетку, атакуемую по вертикали
    board[x][i]++; //указываем на клетку по горизонтали
    
    d = j - x + i; //вычисляем 1 диагональ
    if (d >= 0 && d < 8) { //проверяем, что ферзь не за пределами доски
        board[i][d]++;
        }
    d = j + x - i;//вычисляем 2 диагональ
    
    if (d >= 0 && d < 8) { //проверяем, что ферзь не за пределами доски
        board[i][d]++;
        }
    }
    board[x][j] = queen;
}

void deleteQueen (int x, int j) {
    int d; //диагональ
    for (int i = 0; i < 8; ++i) { //сначала увеличим, потом делаем тело
    board[i][j]--; //указываем на клетку, атакуемую по вертикали
    board[x][i]--; //указываем на клетку по горизонтали
    
    d = j - x + i; //вычисляем 1 диагональ
    if (d >= 0 && d < 8) { //проверяем, что ферзь не за пределами доски
        board[i][d]--;
        }
    d = j + x - i;//вычисляем 2 диагональ
    
    if (d >= 0 && d < 8) { //проверяем, что ферзь не за пределами доски
        board[i][d]--;
        }
    }
    board[x][j] = 0;
}

//на вход достаточно подать номер строки
    bool checkQueen (int i){
        bool result = false;

        for (int j = 0; j < 8; ++j){
            if (board[i][j] == 0){ //чекаем, что королева никого не бьет перед тем, как поставить
                setQueen(i, j); //ставим ферзя и намечаем клетки под боем

                if (i == 7){
                    result = true;
                }
                else if (!(result = checkQueen(i+1))) {
                    deleteQueen (i, j);
                }
            }
            if (result) {break;}
        }
        return result;
}
/////////////////////////
int main () {
    int num;
    cout << "1 - Sum of raws \n" << "2 - Fibonacci numbers \n" << "3 - Hanoi tower \n" << "4 - 8 queens \n" << endl; 
    cout << "Enter a variant: ";
    cin >> num;

    switch (num) {

        case (1): // Сумма ряда: 1 (ln^n(3) / n!) * x^n
        int x, n;
        cout << "Enter a number x: ";
        cin >> x;
        cout << "Enter a number n: ";
        cin >> n;
        cout << sum(n, x);
        break;
    
        case (2): // Фибоначчи
        int a;
        cout << "Enter a number: ";
        cin >> a;
        cout << fib(a);
        break;

        case (3): //ханойская башня
        cout << "Enter number of rings: ";
        cin >> n;
        cout << endl;
        towerOfHanoi(n, '1', '3', '2');
        break;

        case (4): //8 ферзей
        
        //уметь сбросить доску, выводить, чекать ферзя (можно ли поставить), и ставить ферзя
        //void resetBoard();
        //void showBoard();
        //bool checkQueen (int i);
        //void setQueen(int x, int j);
        //void deleteQueen (int x, int j);
        
        int position;
        cout << "Enter a position: ";
        cin >> position;
        resetBoard();
        checkQueen(0);
        showBoard();

        break;

        //в инт мейн запилить случайную пощицию или ввод польщователем клетки
        

    }
    return 0;
}