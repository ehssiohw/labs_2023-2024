Тема: Работа с одномерными массивами
Вариант: 1

Содержание:
Сформировать одномерный массив целых чисел, используя датчик случайных чисел. Распечатать полученный массив. Удалить элемент с номером k. Добавить после каждого четного элемента массива элемент со значением 0. Распечатать полученный массив.
