
/*Удалить элемент с указанным номером, добавить элемент после элемента с указанной фамилией.*/
#include <iostream>
#include <String>
#include <fstream>

using namespace std;

struct Abiturient {
    string name;
    int date;
    int grade;
    double point;

};

void output(const string& filename) {
    ifstream file (filename);
    if (file.is_open()) {
        cout << "File is open. Read the information..." << endl;
        string tmp;

        while (!file.eof()) {
            file >> tmp;
            cout << tmp << endl;
        }
        file.close();
    }
    else {
        cout << "Error" << endl;
    }


}

void del(const string& filename, const string& numberOperation) {
    ifstream file(filename);
    if (file.is_open()) {
        cout << "File is open. Please wait..." << endl;
    
    string tmp = "tmp.txt";
    ofstream tmpFile(tmp);

    if (!tmpFile) {
       cout << "Error (create a temp)." << endl;
    }

    string line;
    bool f = false;

    while (getline(file, line)) {
        if (line != numberOperation) {
            tmpFile << line <<endl;
        } 
        else {
            f = true;
        }
    }

    file.close();
    tmpFile.close();

    if (f) {  // Удаляем исходный файл и переименовываем временный
        remove(filename.c_str());
        rename(tmp.c_str(), filename.c_str());
        cout << "Done" << endl;
    } else {
        remove(tmp.c_str());  // Строка не найдена - удаляем временный файл
        cout << "String not found." << endl;
        }
    }
    else {
        cout << "Err (delete)" << endl;
    }
}

void add (const string& filename, const string& newLine, const string& nowLine) {
    ifstream file(filename);
    string line;
    string tmp;
    while (getline(file, line)){
        tmp += line + " ";
        if (line == nowLine) {
            tmp += newLine + " ";
        }
    }
    file.close();

}
 
int main () {
    setlocale(LC_ALL, "RU");
    Abiturient s;
    
    ofstream file("abi.txt");
    
    int countOfAbiturients;

    cout << "Enter a count of abiturients: ";
    cin >> countOfAbiturients;

    if (countOfAbiturients > 100 && !file.is_open()) {
        cout << "Enter another number or check the file.";
    }

    else {
    for (int i = 0; i < countOfAbiturients; i++) {
        cout << i+1 << ") Surname: ";
        cin >> s.name;
        file << s.name << endl;

        cout << " Date of birth (xxyyzzzz): ";
        cin >> s.date;
        file << s.date << endl;

        cout << " Grade (3): ";
        cin >> s.grade;
        file << s.grade << endl;

        cout << " Point: ";
        cin >> s.point;
        file << s.point << endl;

       
    }


    string choise;
    cout << "Оutput the file? y/n ";
    cin >> choise;
     
     if (choise == "y") {
        output("abi.txt");
     }
     else if (choise == "n") {
        cout << "Ok" << endl;
        file.close();
     }
     else {
        cout << "Choose y or n" << endl;
     }

    
    
    
    string numberOperation;
    cout << "Do you want to delete? y/n";
    cin >> choise;
   
    if (choise == "y") {  
    cout << "Which string to delete?";
    cin >> numberOperation;
        del("abi.txt", numberOperation);
     }
     else if (choise == "n") {
        cout << "Ok" << endl;
        file.close();
     }
     else {
        cout << "Choose y or n" << endl;
     }

    
     cout << "Do you want to add smth? y/n";
     cin >> choise;
     if (choise == "y") {
        string newLine, nowLine;
        cout << "Surname: ";
        cin >> nowLine;
        cout << "Write new string: ";
        cin >> newLine;
        add("abi.txt", newLine, nowLine);     
        }
     else if (choise == "n") {
        cout << "Ok" << endl;
        file.close();
     }
     else {
        cout << "Choose y or n" << endl;
     }
    
}


    return 0;
}