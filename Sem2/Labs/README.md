**1_ЛЕК** - Заполнение двумерного массива, задача с лекции;  
**2_ЛЕК** - Заполнение двумерного массива, задача с лекции;  
**3_РЕК** - Рекурсии: сумма членов ряда, Фибоначчи, 8 ферзей, Ханойские башни;  

###### Далее идет зеленая методичка  
**4_ЗЕЛ** - Формирование одномерного массива;  
**5_ЗЕЛ** - Функции и массивы;  
**6_ЗЕЛ** - Строки;  
**7_ЗЕЛ** - 1 - Перегруженные функции; 2 - Функции с переменным числом параметров;  
**8_ЗЕЛ** - Блоковый ввод-вывод;  
**9_ЗЕЛ** - Строковый ввод-вывод;  
**10_ЗЕЛ** - Динамические массивы;  
**11_ЗЕЛ** - Информационные динамические структуры.  