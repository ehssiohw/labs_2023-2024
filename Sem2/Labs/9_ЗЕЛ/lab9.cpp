//Скопировать в файл F2 только четные строки из F1.
//Подсчитать размер файлов F1 и F2 (в байтах).
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

size_t file1(){
    ifstream F1;
    size_t sizeF1 = 0; //size_t возвращает значние в байтах
    while (!F1.eof()) { //до конца файла
        F1.get(); //считаем по 1 символу
        sizeF1++;
    }
    F1.close();
    return sizeF1;
}

size_t file2(){
    ifstream F2;
    size_t sizeF2 = 0;
    while (!F2.eof()) { //до конца файла
        F2.get(); //считаем по 1 символу
        sizeF2++;
    }
    F2.close();
    return sizeF2;
}

    
int main() {
    ifstream F1("F1.txt"); //вход
    ofstream F2("F2.txt");
    if (F1.is_open() && F2.is_open()){
        cout << "Files aren't open";
    }
    else {
        cout << "Files are open" << endl;
    }
    int line = 1; //ну типа счетчика
    string str;
    while (getline(F1, str)) {
        if (line % 2 == 0){
            F2 << str << endl;
        }
        line++;
        cout << "..." << endl; 
    }
    F1.close();
    F2.close(); //записали - закрыли
    cout << "Files are ready" << endl;
    cout << "Bytes F1: " << file1() << "\nBytes F2: " << file2() << endl;

    return 0;
}

