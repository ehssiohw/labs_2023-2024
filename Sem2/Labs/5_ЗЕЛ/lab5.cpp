#include <iostream>
#include <string>

using namespace std;

string str(char** arr, int rows, int cols) {
  string line;
  for (int i = 0; i < rows; i++) {
    for (int j = 0; j < cols; j++) {
      if (arr[i][j] == '0') {
        if (arr[i][j+1] == '0'){
          j++;
        }
        if (j == cols - 1) {
          line += ")";

        }
        else {
          line += ",";
        }
      }
      if (j == 0) {
        line += "(";
        line += arr[i][j];
      }
      else if (arr[i][j] != '0') {
        line += arr[i][j];
      }
    }
  }
  return line;
}

int main() {
  int rows; //строк
  int cols; //столбцов
  cout << "Enter size MxN \n" << "M: ";
  cin >> rows;
  cout << "N: ";
  cin >> cols;
  char** arr = new char* [rows];
  for (int i = 0; i < rows; i++) {
    arr[i] = new char[cols];
  }
  for (int i = 0; i < rows; i++) {
    for (int j = 0; j < cols; j++) {
      cin >> arr[i][j];
    }
  }
   cout << str(arr, rows, cols);
   return 0;
}