#include <iostream>
#include <string>
using namespace std;
int main()
{
    bool f = false;
    int n;
    string str;
    cout << "Enter sentence: ";
    getline(cin,str);
    n = str.length();
    int i = 0;
    while (i <= (n - 1)) {
        if (str[i] == str[n - 1 - i]) {
            f = true;
        }
        else {
            f = false;
        }
        i++;
    }
    if (f) {
        cout << "Palindrome";
    }
    else {
        cout << "Not a palindrome";
    }
    return 0;
}