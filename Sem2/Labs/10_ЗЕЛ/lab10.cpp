//Сформировать одномерный массив 
//Удалить из него элемент с заданным номером, 
//добавить элемент с заданным номером;
#include <iostream>
#include <ctime>
#include <string>

using namespace std;
int* del(int delElem, int& size, int* arr){
    if (delElem < 0 || delElem >= size) {
        cout << "Err" << endl;
        return arr;
    }

    for (int i = delElem; i < size-1; i++) {
        arr[i] = arr[i+1];
    }
    size--;

    int* newArr = new int[size];
    for (int i = 0; i < size; i++) {
        newArr[i] = arr[i];
    }
    delete[] arr;

    return newArr;
}

int* add(int addElem, int& size, int place, int* arr) {
   if (place < 0 || place >= size) {
        cout << "Err" << endl;
        return arr;
    } 

    int* newArr = new int[size + 1]; 
    for (int i = 0; i < place; i++) {
        newArr[i] = arr[i]; 
    }
    newArr[place] = addElem; 
    for (int i = place + 1; i < size + 1; i++) {
        newArr[i] = arr[i - 1]; 
    }
    size++; 
    delete[] arr; 
    return newArr;
}

int main () {
    srand(time(0));
    int size;
    cout << "Enter size: ";
    cin >> size;
    int* arr = new int[size]; //одномерный динамический массив. Поздравляем!)

    for (int i = 0; i < size; i++) {
        arr[i] = rand()%100+1; 
    }

    for (int i = 0; i < size; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;

    string choise;
    cout << "Do you want to delete something? y/n ";
    cin >> choise;

    if (choise == "y") {
    int delElem;
    cout << "Enter an index of element to delete: ";
    cin >> delElem;
    arr = del(delElem, size, arr);

    for (int i = 0; i < size; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;
    delete [] arr;
    }
    else if (choise == "n") {
        cout << "Ok" << endl;
    }
    else {
        cout << "Err";
    }
    
    cout << "Do you want to add something? y/n ";
    cin >> choise;

    if (choise == "y") {
    int addElem, indexElem;

    cout << "Enter an index of element to add: ";
    cin >> indexElem;
    
    cout << "Enter a number: ";
    cin >> addElem;
    arr = add(addElem, size, indexElem, arr);

    for (int i = 0; i < size; i++) {
        cout << arr[i] << " ";
    }
    //cout << endl;
    delete [] arr;
    }
    else if (choise == "n") {
        cout << "Ok" << endl;
    }
    else {
        cout << "Err";
    }

    return 0;
}