//Перевернуть часть массива от p до q (остальной массив должен быть прежним)
#include <iostream>
using namespace std;
int main () {
    int n = 10;
    int arr[n] = {1,2,3,4,5,6,7,8,9,10};
    int tmp, p = 2, q = 5;
    for (int i = 0; i < n; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;
    while (p < q) {
        tmp = arr[p];
        arr[p] = arr[q];
        arr[q] = tmp;
        p++;
        q--;
    }
    for (int i = 0; i < n; i++) {
        cout << arr[i] << " ";
    }
    return 0;
}