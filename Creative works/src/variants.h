#ifndef VARIANTS_H
#define VARIANTS_H
#include <QDialog>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <QMessageBox>
#include <QSqlTableModel>
#include <QSqlQuery>
#include <QCheckBox>
#include <QHBoxLayout>
#include <QListWidget>
#include <QListWidgetItem>

#include <QDialog>

namespace Ui {
class variants;
}

class variants : public QDialog
{
    Q_OBJECT

public:
    explicit variants(QWidget *parent = nullptr);
    ~variants();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

private:
    Ui::variants *ui;

};

#endif // VARIANTS_H
