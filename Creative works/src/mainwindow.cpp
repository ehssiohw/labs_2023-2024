#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFont>
#include <QFontDatabase>
#include <QCoreApplication>
#include <QDesktopWidget>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->lineEdit, &QLineEdit::returnPressed, this, &MainWindow::on_pushButton_clicked);

    // Устанавливаем флаг для зафиксированного размера окна
    setFixedSize(430, 220);

    QLinearGradient gradient(0, 0, 0, this->height());
    gradient.setColorAt(1, QColor(75, 0, 130));
    gradient.setColorAt(0, QColor(218, 112, 214));

    QPalette pal = palette();
    pal.setBrush(QPalette::Window, QBrush(gradient));
    this->setAutoFillBackground(true);
    this->setPalette(pal);

    QString buttonStyle = "background-color: rgb(216, 191, 216);";
    ui->pushButton->setStyleSheet(buttonStyle);
    ui->lineEdit->setStyleSheet(buttonStyle);


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QString login = ui->lineEdit->text();
    if (login == "ИВТ-23-1Б" || login == "ивт-23-1б" || login == "1") {
        hide();
        Students table;
        table.setModal(true);
        table.exec();
    }
    else {
        QMessageBox::critical(this, "Ошибка", "Введена неверная группа");
        ui->lineEdit->clear();
    }
}
