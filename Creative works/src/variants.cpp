#include "variants.h"
#include "ui_variants.h"
#include <QDesktopServices>
#include <QUrl>

variants::variants(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::variants)
{
    ui->setupUi(this);
    connect(ui->pushButton, &QPushButton::clicked, this, &variants::on_pushButton_clicked);

    QString buttonStyle1 = "background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #D8BFD8, stop: 1 #DA70D6);";
    QString buttonStyle2 = "background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #DA70D6, stop: 1 #EE82EE);";
    QString buttonStyle3 = "background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #EE82EE, stop: 1 #FF69B4);";
    QString buttonStyle4 = "background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #FF69B4, stop: 1 #FF1493);";
    QString buttonStyle5 = "background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #FF1493, stop: 1 #DB7093);";
    QString buttonStyle6 = "background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #DB7093, stop: 1 #BA55D3);";

    ui->pushButton->setStyleSheet(buttonStyle1);
    ui->pushButton_2->setStyleSheet(buttonStyle2);
    ui->pushButton_3->setStyleSheet(buttonStyle3);
    ui->pushButton_4->setStyleSheet(buttonStyle4);
    ui->pushButton_5->setStyleSheet(buttonStyle5);
    ui->pushButton_6->setStyleSheet(buttonStyle6);
}

variants::~variants()
{
    delete ui;
}

void variants::on_pushButton_clicked()
{
    QString filePath = "D:/tvorcheskaya/img/osennijjdosmeny.xlsx"; // Укажите путь к вашему файлу Excel
    QDesktopServices::openUrl(QUrl::fromLocalFile(filePath));
}


void variants::on_pushButton_2_clicked()
{
    QString filePath = "D:/tvorcheskaya/img/osennijj  posle smeny.xlsx"; // Укажите путь к вашему файлу Excel
    QDesktopServices::openUrl(QUrl::fromLocalFile(filePath));
}


void variants::on_pushButton_3_clicked()
{
    QString filePath = "D:/tvorcheskaya/img/osennijj  sessiya.xlsx"; // Укажите путь к вашему файлу Excel
    QDesktopServices::openUrl(QUrl::fromLocalFile(filePath));
}


void variants::on_pushButton_4_clicked()
{
    QString filePath = "D:/tvorcheskaya/img/vesennijj  do smeny.xlsx"; // Укажите путь к вашему файлу Excel
    QDesktopServices::openUrl(QUrl::fromLocalFile(filePath));
}


void variants::on_pushButton_5_clicked()
{
    QString filePath = "D:/tvorcheskaya/img/vesennijj  posle smeny.xlsx"; // Укажите путь к вашему файлу Excel
    QDesktopServices::openUrl(QUrl::fromLocalFile(filePath));
}


void variants::on_pushButton_6_clicked()
{
    QString filePath = "D:/tvorcheskaya/img/vesennijj  sessiya.xlsx"; // Укажите путь к вашему файлу Excel
    QDesktopServices::openUrl(QUrl::fromLocalFile(filePath));
}

