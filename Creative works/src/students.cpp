#include "students.h"
#include "ui_students.h"
#include "variants.h"
#include <QDesktopServices>

Students::Students(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Students)
{
    ui->setupUi(this);
    setFixedSize(1000, 795);
    ui->dateEdit->setDate(QDate::currentDate());
    /************ОФОРМЛЕНИЕ ЦВЕТОМ************/
    QLinearGradient gradient(0, 0, 0, this->height());
    gradient.setColorAt(1, QColor(75, 0, 130));
    gradient.setColorAt(0, QColor(218, 112, 214));

    QPalette pal = palette();
    pal.setBrush(QPalette::Window, QBrush(gradient));
    this->setAutoFillBackground(true);
    this->setPalette(pal);

    QString purpleColor = "background-color: rgb(216, 191, 216);";
    ui->pushButton->setStyleSheet(purpleColor);
    ui->tableView->setStyleSheet(purpleColor);
    ui->listWidget->setStyleSheet(purpleColor);
    ui->pushButton_2->setStyleSheet(purpleColor);
    ui->pushButton_3->setStyleSheet(purpleColor);
    ui->pushButton_4->setStyleSheet(purpleColor);
    ui->pushButton_5->setStyleSheet(purpleColor);
    ui->comboBox->setStyleSheet(purpleColor);
    ui->dateEdit->setStyleSheet(purpleColor);
    ui->textEdit->setStyleSheet(purpleColor);
    ui->pushButton_6->setStyleSheet(purpleColor);
    ui->pushButton_7->setStyleSheet(purpleColor);
    ui->pushButton_8->setStyleSheet(purpleColor);

    /************************************/

    /************ЗАПРОСЫ ДЛЯ БАЗЫ************/
    database = QSqlDatabase::addDatabase("QSQLITE"); //указываем какой язык используем
    database.setDatabaseName("./DataBase.db"); //указываем базу и ее путь (создавать не обязательно)
    if (database.open()) { //если подключение выполнено
        QMessageBox::information(this, "База данных", "Приветствуем, Софья Усламина!\nБаза данных " + database.databaseName() + " подключена");

        //устанавливаем модель, через которую будем взаимодействовать
        model = new QSqlTableModel(this, database);
        model->setTable("Students"); //выбираем таблицу из существующей базы
        model->lastError();
        model->select(); //выводим содержимое
        ui->tableView->setModel(model); //выводим содержимое в таблицу

        model->setHeaderData(1, Qt::Horizontal, "Фамилия", Qt::DisplayRole); //задаем заголовки и выравниваем их
        model->setHeaderData(2, Qt::Horizontal, "Имя", Qt::DisplayRole);

        ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
        ui->tableView->setColumnHidden(0, true);
        ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);

        QComboBox *comboBox = ui->comboBox; //создаем выпадающий список
        QSqlQuery query("SELECT name FROM Disciplines"); //содержимое списка берем из базы
        while (query.next()) { //проходимся по результатам запроса
            QString value = query.value(0).toString(); //получаем значение и преобразуем в строку
            comboBox->addItem(value); //добавляем в список
        }


        query.exec("SELECT name, surname FROM students"); //выполняем функцию выборки имени и фамилии из базы студентов
        while (query.next()) { //проходимся по результатам
            QString name = query.value(0).toString(); //преобразуем в строки
            QString surname = query.value(1).toString();

            QString fullName = " " + name + " " + surname; //просто сложение имени и фамилии
            QWidget* customWidget = new QWidget; //кастомный виджет для отображения чекбокса и студента
            QHBoxLayout* layout = new QHBoxLayout(customWidget);  //распологаем горизонтально
            QListWidget *listWidget = ui->listWidget;
            QLabel* label = new QLabel(fullName);
            QCheckBox* checkBox = new QCheckBox();

            layout->addWidget(label);
            layout->addSpacing(10);
            layout->addWidget(checkBox);

            layout->setContentsMargins(0, 0, 0, 0);
            customWidget->setLayout(layout);

            QListWidgetItem* item = new QListWidgetItem(listWidget);
            listWidget->setItemWidget(item, customWidget);
        }
    }

    else { // иначе если подключение не выполнено, вывести ошибку
        QMessageBox::critical(this, "База данных", "База данных " + database.databaseName() + " не подключена" + database.lastError().databaseText());
    }

}

Students::~Students()
{
    delete ui;
}

//добавляем новую строку
void Students::on_pushButton_clicked()
{
    model->insertRow(model->rowCount());

}

//удаляем и обновляем
void Students::on_pushButton_2_clicked()
{
    model->removeRow(currentRow);
    model->select();
}

//сохранение номера строки по выбранной строке на виджете
void Students::on_tableView_clicked(const QModelIndex &index)
{
    currentRow = index.row();
}

//переводим имя в номер
int Students::getStudentId(const QString& name) {
    QSqlQuery query;
    //подготавливает запрос для выборки идентификатора студента по его имени. Псевдоним :name используется для подстановки имени студента в запрос.
    query.prepare("SELECT id FROM Students WHERE name = :name");
    //устанавливает значение псевдонима :name в запросе на имя студента, которое передается в функцию в качестве параметра.
    query.bindValue(":name", name);
    //выполняет запрос и проверяет, был ли получен хотя бы один результат.
    if (query.exec() && query.next()) {
        return query.value(0).toInt(); //возвращаем идентификатор студента
    }

    // В случае ошибки или если человек с таким именем не найден, возвращаем -1
    return -1;
}

//переводим предмет в номер (то же самое)
int Students::getDisciplineId (const QString& subjectName) {
    QSqlQuery query;
    query.prepare("SELECT id FROM Disciplines WHERE name = :name");
    query.bindValue(":name", subjectName);

    if (query.exec() && query.next()) {return query.value(0).toInt();}
    return -1;
}

//отправляем отмеченные данные в базу
void Students::on_pushButton_3_clicked() {
    QDate selectedDate = ui->dateEdit->date(); // Получаем выбранную дату
    QString selectedSubject = ui->comboBox->currentText(); // Получаем выбранный предмет
    int totalStudents = 0;
    int attendedStudents = 0;

    // Проходимся по всем элементам списка "listWidget"
    for (int i = 0; i < ui->listWidget->count(); ++i) {
        QListWidgetItem* item = ui->listWidget->item(i); // Получаем i-ый элемент списка
        QWidget* widget = ui->listWidget->itemWidget(item); // Получаем виджет, связанный с элементом списка
        QLabel* label = widget->findChild<QLabel*>();  // Находим виджет QLabel (отображает текст) внутри виджета элемента списка
        QCheckBox* checkBox = widget->findChild<QCheckBox*>(); // Находим виджет QCheckBox (флажок) внутри виджета элемента списка

        // Если флажок найден
        if (checkBox) {
            totalStudents++; // Увеличиваем общее количество студентов

            // Если флажок отмечен
            if (checkBox->isChecked()) {
                attendedStudents++; // Увеличиваем количество присутствующих студентов

                QString fullName = label->text(); // Получаем полный текст из QLabel (ФИО студента)
                QString name = fullName.split(" ").at(1); // Извлекаем фамилию студента

                QSqlQuery insertQuery; // Создаем объект SQL-запроса
                insertQuery.prepare("INSERT INTO Attendance (date, student_id, discipline_id, attended) VALUES (:date, :student_id, :discipline_id, 2)");
                insertQuery.bindValue(":date", selectedDate); // Дата
                insertQuery.bindValue(":student_id", getStudentId(name)); // ID студента
                insertQuery.bindValue(":discipline_id", getDisciplineId(selectedSubject)); // ID предмета
                insertQuery.exec(); // Выполняем запрос
            }
        }
    }

    // Вычисляем процент посещаемости
    int attendancePercentage = 0;
    if (totalStudents > 0) {
        attendancePercentage = 100 - ((attendedStudents * 100) / totalStudents);
    }

    // Выводим результат на textEdit
    ui->textEdit->setText("Процент посещаемости: " + QString::number(attendancePercentage) + "%");

    QMessageBox::information(this, "Отправка данных", "Данные успешно отправлены.");

    // Сбрасываем состояние флажков
    for (int i = 0; i < ui->listWidget->count(); ++i) {
        QListWidgetItem* item = ui->listWidget->item(i);
        QWidget* widget = ui->listWidget->itemWidget(item);
        QCheckBox* checkBox = widget->findChild<QCheckBox*>();

        if (checkBox) {
            checkBox->setChecked(false);
        }
    }
}



void Students::on_pushButton_4_clicked()
{
    QString selectedSubject = ui->comboBox->currentText(); // Получаем выбранный предмет
    QListWidgetItem* selectedItem = ui->listWidget->currentItem();  //получаем студента

    // Проверяем, выбран ли студент
    if (!selectedItem) {
        QMessageBox::warning(this, "Ошибка", "Студент не выбран.");
        return; // Выходим из функции, если студент не выбран
    }

    QWidget* widget = ui->listWidget->itemWidget(selectedItem); // Получаем виджет, связанный с выбранным студентом
    QLabel* label = widget->findChild<QLabel*>(); //Считываем текст внутри виджета
    QString fullName = label->text(); // Получаем полный текст из QLabel (ФИО студента)
    QString name = fullName.split(" ").at(1); // Извлекаем фамилию студента, разделяя ФИО по пробелам
    int studentId = getStudentId(name); // Получаем ID студента по фамилии
    int disciplineId = getDisciplineId(selectedSubject); // Получаем ID предмета по названию

    // Проверяем, найдены ли ID студента и предмета
    if (studentId == -1 || disciplineId == -1) {
        QMessageBox::warning(this, "Ошибка", "Студент или предмет не найден.");
        return; // Выходим из функции, если не найдены
    }

    QSqlQuery query;  // Создаем объект SQL-запроса
    // Подготавливаем запрос для получения количества пропусков из таблицы посещаемости
    query.prepare("SELECT attended FROM Attendance WHERE student_id = :student_id AND discipline_id = :discipline_id");

    // Привязываем ID студента и предмета к переменным в запросе
    query.bindValue(":student_id", studentId);
    query.bindValue(":discipline_id", disciplineId);

    // Выполняем запрос и проверяем, получены ли данные
    if (query.exec() && query.next()) {
        int absences = query.value(0).toInt();
        // Получаем количество пропусков из результата запроса

        QMessageBox::information(this, "Количество пропусков", "Студент " + name + " имеет " + QString::number(absences) +
                                 " пропусков по предмету: " + selectedSubject);
    } else {
        QMessageBox::warning(this, "Ошибка", "Информация о пропусках не найдена.");
    }
}


void Students::on_pushButton_5_clicked()
{
    variants timetable;
    timetable.setModal(true);
    timetable.exec();
}


void Students::on_pushButton_7_clicked()
{
    QDesktopServices::openUrl(QUrl("https://vk.com/dekanatetf"));
}


void Students::on_pushButton_6_clicked()
{
    QDesktopServices::openUrl(QUrl("https://pstu.ru/"));
}


void Students::on_pushButton_8_clicked()
{
    QDesktopServices::openUrl(QUrl("https://pstu.ru/basic/glossary/staff/"));
}

