#ifndef STUDENTS_H
#define STUDENTS_H

#include <QDialog>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <QMessageBox>
#include <QSqlTableModel>
#include <QSqlQuery>
#include <QCheckBox>
#include <QHBoxLayout>
#include <QListWidget>
#include <QListWidgetItem>

namespace Ui {
class Students;
}

class Students : public QDialog
{
    Q_OBJECT

public:
    explicit Students(QWidget *parent = nullptr);
    ~Students();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_tableView_clicked(const QModelIndex &index);

    void on_pushButton_3_clicked();
    int getStudentId(const QString& name);
    int getDisciplineId (const QString& subjectName);
    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_8_clicked();

private:
    Ui::Students *ui;
    QSqlDatabase database;
    QSqlTableModel *model;
    QSqlQuery *query;

    int currentRow;
};

#endif // STUDENTS_H
