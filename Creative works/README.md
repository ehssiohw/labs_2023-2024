## Творческая работа по теме АРМ специалиста и решение задачи Коммивояжера ##  
:blush:  _**Как установить: скачать папку src, в Qt запустить .pro**_ :blush:  

YouTube: https://youtu.be/hQe2hcQnHnU

### Анализ задачи  
###### Задача Коммивояжера  
подробнее см. [здесь](https://gitlab.com/ehssiohw/labs_2023-2024/-/tree/main/Sem2/Graph)  

1. Инициализация. Выбрать стартовый город. Создать начальное ограничение для оптимального пути.
2. Разбить задачу на подзадачи, создавая дерево решений. Для каждой подзадачи выберите следующий город для посещения.
3. Оценить нижнюю и верхнюю границы для каждой подзадачи и  определить, стоит ли исследовать эту часть или можно отсечь. Если нижняя граница превышает текущий лучший результат, то отсечь.
5. Продолжать разветвляться и оценивать подзадачи до тех пор, пока не будет найдено оптимальное решение или не будут отсечены все невозможные варианты.

###### АРМ Старосты  

1. Необходимо, чтобы программа напряпую взаимодействовала с базой: insert, select, delete, update, query.  
2. Прописать в коде создание базы данных. Для внутреннего взаимодействия с базой необходим [DB Browser](https://sqlitebrowser.org/)
3. Создать 4 таблицы: список группы и их идентификаторы (Students), список дисциплин и их идентификаторы (Disciplines), список отсутствующих с идентификатором студента, идентификатором дисциплины, датой (Attendance).   
4. Вручную заполнить базу Students и Disciplines.  
5. Возможность апгрейта: добавим авторизацию пока что по группе. В дальнейшем можно увеличить количество групп.  
6. Во втором окне основная работа после успешной авторизации:  
- список группы с возможностью добавлять и удалять;  
- список предметов;  
- дата;  
- лист виджетов студенты с чекбоксами - для отметки присутствия.  
7. После активации чекбоксов необходимо отправить это количество, дату и конкретно этих людей в базу. Кодом переводим полученное имя и наименование дисциплины в их идентификаторы в базе и отправляем в базу Attendance.  
8. Чтобы узнать количество пропусков на предмете у человека - отметить на чекбоксе, выбрать дату и предмет. Нажать кнопку, которая из базы Attendance покажет в отдельном окне количество пропусков.  

### Демонстрация работы программы задача Коммивояжера
![z1](https://sun9-15.userapi.com/impg/E1t8zFmEgUOm-QST5UrXhFIMy5HI_B9ssF_J6A/J3Nrn60zm68.jpg?size=801x806&quality=96&sign=59dc29fc787de2b50135286904d8c3cd&type=album)  
![z2](https://sun9-76.userapi.com/impg/tpgyOhc3elcr2oJDrQb1-_hJ5tpCjVMAZ1R_-w/Hc3B3Qs6kQg.jpg?size=495x372&quality=96&sign=369f0b1c69f97488ce13a65f789dad64&type=album)  
![z3](https://sun9-25.userapi.com/impg/s1vRzSHS2Fn-5KpSEUpM8pMZUA5YjsPrn8-c4Q/AEMBvyca1Pk.jpg?size=359x143&quality=96&sign=ea3e1a41a1aaf6315cc8597f93c03dc9&type=album)  
![zz1](https://sun9-73.userapi.com/impg/PFbh_FuG12NBsbA8ujUXk0azG-rpQSPdxjx_oA/ZYYGbmSTRX4.jpg?size=434x510&quality=96&sign=1500cecc84f45c0b8098fa36048d8298&type=album)
![zz2](https://sun9-58.userapi.com/impg/6WIJokrZIzIPRthKfQaAFGXgxtWJx7cIs2JDLw/LuSgqYmwdYE.jpg?size=549x447&quality=96&sign=a7511a25f57a25d213b09ee2c7bd7e5f&type=album)
![zz3](https://sun9-78.userapi.com/impg/TbzZweBlrtll__aUGaPnc5kD5zWDR7me5Yq80A/k6gwZrYmEQ0.jpg?size=362x141&quality=96&sign=6881ee05c25cd09a5b44b78b11d6bcb2&type=album)

### Демонстрация работы программы АРМ Старосты  
Сама программа:  
![gif1](https://vk.com/doc440424702_676608715?hash=4PccnwDtlL2PUkmQNqPoq2nLMRgAaxKVVguCC6hpldP&dl=jBE7zZyBzXRLiNltURS9IZzklkLslJs3zgcloML3mnT&wnd=1&module=im)  

База данных:  
![gif2](https://vk.com/doc440424702_676608745?hash=AcVg63LZ3C0kyv8DkkCE0zsNQtEBQ7IzATM7Y22fHrP&dl=yk8sn0liC79cFmhPR76sPjl9e5JSQi5dqFBzdXOsmsH&wnd=1&module=im) 

Основной функционал. Создание и удаление студента.  
![gif3](https://vk.com/doc440424702_676608808?hash=HGnS0yrSH05Q4S24wldoLBCxLEmu7NwWuztFZkjmoxw&dl=A0hqHwR2frif7xl014YFX5whXTd5OYV02Y4udvJQ7CP&wnd=1&module=im)  

Отметка отсутствующих и проверка пропусков.  
![gif4](https://vk.com/doc440424702_676608864?hash=ed08llw7A6zD32gRtPXLSNjkdjdqzms1p7goL66OMik&dl=Hxs5ev31FHUZXvAInzQAKmreIi3S5Bv1mvEUueXEPT8&wnd=1&module=im)  

Дополнительные кнопки.  
![gif5](https://vk.com/doc440424702_676608910?hash=44vp2S4AGLj1YzAkzyQ2wRTgQg9pzPULNDSHSKm7biH&dl=OgQQjpp86p08hVAXm7QweBaOtoSysEliTMCUMb21Yuw&wnd=1&module=im)

###### UML-диаграмма по задаче Коммивояжера 
![uml](https://psv4.userapi.com/c237331/u440424702/docs/d22/8b415164e52f/1.png?extra=dOw1o18uA3w-dHQoZ7aF0BY9RdCqrEJAsticQyF6G6C-wUp2IHdYG29L6gpJ9CaXmDKcaT_kUaDqNCXpOOCt-HTFr-nDrXvx_SfUhl_hEH9NbBr5Hok2FihWEh2bwIfoTonMA01OMnI9dDGeScWXpfeCCw)

###### UML-диаграмма по АРМ Старосты (учет посещаемости)
![uml2](https://sun9-47.userapi.com/impg/X_lGVgrHn575mAg3YTXPlLbOJyidZV-hBW3S1g/NHVVbkQuVsM.jpg?size=591x299&quality=96&sign=8f89ac3c4d2e75bb1321eee339f05440&type=album)