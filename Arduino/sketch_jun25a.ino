long distance = 0;
#define LeftMotorForw 6 //Левый (А-1A) мотор вперед
#define LeftMotorBack 5 //Левый (А-1B) мотор назад
#define RightMotorForw 10 //Правый (В-1A) мотор вперед
#define RightMotorBack 9 //Правый (В-1B) мотор назад

#define minSpeed 120
#define midSpeed 180
#define maxSpeed 255

//дальномер
int trig = 8;
int echo = 7;

//энкодеры
#define leftEnc 2
#define rightEnc 3
volatile uint16_t leftImpulse = 0;
volatile uint16_t rightImpulse = 0;
void leftEncInt() {leftImpulse++;}
void rightEncInt() {rightImpulse++;}

#define stepCM 0.55 //шаг перемещения в см
#define impAngle2 0.2 //число импульсов на 1 градус разворота
#define impAngle4 0.4
#define toCm 58.2
/*************************************************************/
void setup(){
  Serial.begin(9600);
  pinMode (RightMotorForw, OUTPUT);
  pinMode (RightMotorBack, OUTPUT);
  pinMode (LeftMotorForw, OUTPUT);
  pinMode (LeftMotorBack, OUTPUT);
  pinMode(trig, OUTPUT);
  pinMode(echo, INPUT);

  attachInterrupt (digitalPinToInterrupt(leftEnc), leftEncInt, CHANGE);
  attachInterrupt (digitalPinToInterrupt(rightEnc), rightEncInt, CHANGE);

  turnRadR(90, midSpeed);
  moveStop();
  turnRadR(180, midSpeed);
  moveStop();
  turnRadR(360, midSpeed);
  moveStop();
  turnRadR(45, midSpeed);
  moveStop();
  
  turnRadL(90, midSpeed);
  moveStop();
  turnRadL(180, midSpeed);
  moveStop();
  turnRadL(360, midSpeed);
  moveStop();
  turnRadL(45, midSpeed);
  moveStop();

  turnLeft(90, midSpeed);
  moveStop();
  turnLeft(180, midSpeed);
  moveStop();
  turnLeft(360, midSpeed);
  moveStop();
  turnLeft(45, midSpeed);
  moveStop();

  turnRight(90, midSpeed);
  moveStop();
  turnRight(180, midSpeed);
  moveStop();
  turnRight(360, midSpeed);
  moveStop();
  turnRight(45, midSpeed);
  moveStop();

}

/*************************************************************/
void loop(){
    if (getDistance() > 20) {
    moveRobot(minSpeed, minSpeed);
    delay(1000);
    moveRobot(midSpeed, midSpeed);
    delay(1000);
    moveRobot(maxSpeed, maxSpeed);
    delay(1000);
    moveStop();
    
    float wayForw = rightImpulse * stepCM;
    Serial.print("\nПуть вперед: "); Serial.print(wayForw);
    Serial.print("\nRight Impulse: "); Serial.print(rightImpulse);
    int tmp = rightImpulse;
    turnRight(180, midSpeed); 

      rightImpulse = 0;
      float wayBack = 0;
      while (wayForw >= wayBack || tmp >= rightImpulse) {
        wayBack = rightImpulse * stepCM;
        moveRobot(midSpeed, midSpeed-30);
        Serial.print("\nПуть назад: "); Serial.print(wayBack);
        Serial.print("\nRight Impulse: "); Serial.print(rightImpulse);
    }
    moveStop();
    } else {
      moveStop();
      turnRight(90, midSpeed);}
 }
/*************************************************************/
void moveRobot(int16_t speedLeft, int16_t speedRight) {
  if (speedLeft > 0) {
    analogWrite(LeftMotorForw, constrain(speedLeft, minSpeed, maxSpeed));
    analogWrite(LeftMotorBack, 0);
  } else if (speedLeft < 0) {
    analogWrite(LeftMotorForw, 0);
    analogWrite(LeftMotorBack, constrain(abs(speedLeft), minSpeed, maxSpeed));
  } else {
    analogWrite(LeftMotorForw, 0);
    analogWrite(LeftMotorBack, 0);
  }
  if (speedRight > 0) {
    analogWrite(RightMotorForw, constrain(speedRight, minSpeed, maxSpeed));
    analogWrite(RightMotorBack, 0);
  } else if (speedRight < 0) {
    analogWrite(RightMotorForw, 0);
    analogWrite(RightMotorBack, constrain(abs(speedRight), minSpeed, maxSpeed));
  } else {
    analogWrite(RightMotorForw, 0);
    analogWrite(RightMotorBack, 0);
  }
}

void moveForward() {
  distance = getDistance();
  Serial.print("\nDistance: "); Serial.print(distance);
  if (distance > 18) {  
    for (int speed = minSpeed; speed < maxSpeed; speed++) {
      moveRobot(speed, speed-30);
      delay(10);
    }
    moveStop();
  } else {
    Serial.print("Обнаружено препятствие при попытке ехать вперед");
    moveStop();
   } 
}
void moveBack() {
  for (int speed = minSpeed; speed < maxSpeed; speed++) {
     moveRobot(-speed, -(speed-30));
     delay(10);
    }
  moveStop();
}

void moveStop() {
  moveRobot(0, 0);
  delay(1000);
}

// Определение дистанции до объекта в см
long getDistance() {
  long distance_cm = getEchoTiming() / toCm;
  return distance_cm;
}

// Определение времени задержки
long getEchoTiming() {
  digitalWrite(trig, LOW);
  delayMicroseconds(2);
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);  
  digitalWrite(trig, LOW);
  long timing = pulseIn(echo, HIGH); 
  return timing;
}


void turnRight(uint16_t angle, uint8_t speed) {
  distance = getDistance();
  if (distance > 18) {
  leftImpulse = 0;
  rightImpulse = 0;
  moveRobot(speed, -(speed-30));
  while (leftImpulse < angle * impAngle2 || rightImpulse < angle * impAngle2) {
//    Serial.print("\tLeft Encoder: "); Serial.print(leftImpulse);
//    Serial.print("\nRight Encoder: "); Serial.print(rightImpulse);    
  }
  moveStop();
  } else {
    Serial.print("\nОбнаружено препятствие при попытке повернуть направо");
    moveStop();
    }
}

void turnLeft(uint16_t angle, uint8_t speed){
  distance = getDistance();
  if (distance > 18) {
  leftImpulse = 0;
  rightImpulse = 0;
  moveRobot(-speed, speed-30);
  while (leftImpulse < angle * impAngle2 || rightImpulse < angle * impAngle2) {
    Serial.print("\nЛевый энкодер: "); Serial.print(leftImpulse);
    Serial.print("\nПравый энкодер: "); Serial.print(rightImpulse);    
  }
  moveStop();
  } else {
    Serial.print("\nОбнаружено препятствие при попытке повернуть налево");
    moveStop();
    }
}

void turnRadL (uint16_t radius, uint8_t speed){
  distance = getDistance();
  if (distance > 18) {
  rightImpulse = 0;
  moveRobot(0, speed-30);
  while (rightImpulse < radius * impAngle4) {
    Serial.print("\nПравый энкодер: "); Serial.print(rightImpulse);    
  }
  moveStop();
  } else {
    Serial.print("\nОбнаружено препятствие при попытке повернуть налево (rad)");
    moveStop();
   }
}

void turnRadR (uint16_t radius, uint8_t speed){
  distance = getDistance();
  if (distance > 18) {
  leftImpulse = 0;
  moveRobot(speed, 0);
  while (leftImpulse < radius * impAngle4) {
    Serial.print("\nЛевый энкодер: "); Serial.print(leftImpulse);   
  }
  moveStop();
  } else {
    Serial.print("\nОбнаружено препятствие при попытке повернуть направо (rad)");
    moveStop();
   }
}
