#include <iostream>
#include <cmath>
using namespace std;

int main() {
    int a, N;
    cout << "Enter the variant. 1 - A, 2 - B, 3 - C: ";
    cin >> a;
    switch (a) {

    case 1:
        cout << "Enter a natural number:";
        cin >> N;
        if (N >= 0 && sqrt(N) == floor(sqrt(N))) { //proverka na izvle4enie korn9
            for (int i = 0; i < sqrt(N); i++) {
                for (int j = 0; j <= sqrt(N); j++) {
                    cout << "* ";
                }
                cout << endl;
            }
        }
        else {
            cout << "You need in another number" << endl;
        }
        break;

    case 2:
        cout << "Enter a natural number:";
        cin >> N;
        if (N >= 2) {
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    cout << "* ";
                }
                cout << endl;
            }
        }
        else {
            cout << "You need in another number" << endl;
        }
        break;

    case 3:
        cout << "Enter a natural number:";
        cin >> N;
        if (N >= 2) {
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    if (i == 0 || i == (N - 1) || j == 0 || j == (N - 1)) {
                        cout << "*";
                    }
                    else {
                        cout << " ";
                    }

                }
                cout << endl;
            }

        }
        else {
            cout << "You need in another number" << endl;
        }
    }
    return 0;
}
