//Вычислить сумму S = -1 + 2 - 3 + 4 - 5 + 6 - ... до N слагаемых.
#include <iostream>
using namespace std;

int main () {
    int N, S = 0, j = -1;
    cout << "Enter a count of terms: ";
    cin >> N;
    for (int i = 1; i <= N; ++i) {
        S += j * i;
        j *= -1;
    }
    cout << "Sum: " << S << endl;
    return 0;
}
