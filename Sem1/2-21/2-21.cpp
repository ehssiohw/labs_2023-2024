//Числа вводятся с клавиатуры до тех пор, пока не встретится число 0 (0 — признак окончания ввода). 
//Проверить, упорядочены ли числа по возрастанию.
#include <iostream>
using namespace std;
int main() {
    bool f = true;
    int n, tmp;
    cout << "Enter numbers (0 for stop): ";
    cin >> n;
    while (true) {
        cout << "Enter numbers (0 for stop): ";
        cin >> tmp;
        if (tmp == 0) {
            break;
        }
        else {
            continue;
        }
        if (tmp < n) {
            f = false;
            break;
        }
        n = tmp;
    }

    if (f) {
        cout << "The numbers are in ordered";
    } else {
        cout << "The numbers are not in ordered";
    }

    return 0;
   
}