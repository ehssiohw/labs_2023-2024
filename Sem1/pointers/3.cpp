/*Сложение двух чисел с плавающей запятой через указатели*/
#include <iostream>
using namespace std;
int main () {
    float a, b, sum = 0, *ptr2, *ptr1; 
    cout << "Enter the 1st number: ";
    cin >> a;
    cout << "Enter the 2nd number: ";
    cin >> b;
    ptr1 = &a; //присваиваем адрес а переменной птр1
    ptr2 = &b; 
    sum = *ptr1 + *ptr2;
    cout << sum << endl;
    return 0;
}