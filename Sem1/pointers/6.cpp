/*Подсчитать количество гласных и согласных в строке через указатели 
(в строке только буквы русского алфавита)*/
#include <iostream>
#include <string>
#include <wctype.h>
int main () {
int k, j;
std::string st;
j = k = 0;
std::cout << "Enter a string: " << std::endl;
std::wcin >> st; //w-широкие символы, где под символ выполняется больше 1 байта
const wchar_t *pt = st.c_str();
for (int i = 0; i < st.length(); i++){
    if (tolower(*pt) == 'а' || tolower(*pt) == 'ы' || tolower(*pt) == 'e' || tolower(*pt) == 'ё' || tolower(*pt) == 'у' || tolower(*pt) == 'э' || tolower(*pt) == 'ю' || tolower(*pt) == 'я'){
k++;
    }
    else {
        j++;
    }
}
std::cout << "Count of vowels: " << k << std::endl;
std::cout << "Count of consonants: " << j << std::endl;

return 0;
}