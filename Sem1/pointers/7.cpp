/*Изменение значения переменной через указатель*/
#include <iostream>
using namespace std;
int main () {
    int n = 2;
    int *ptr = &n;
    cout << "Now N = " << n << endl;
    cout << "Enter the new number N: " << endl;
    cin >> *ptr;
   cout << "New N = " << n;
     
return 0;
}