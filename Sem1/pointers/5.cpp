/*Вычислить факториал числа N через указатели*/
#include <iostream>
using namespace std;
int main () {
    int n, f=1;
    int *ptr = &f;
    cout << "Enter the number: ";
    cin >> n;
    for (int i = 1; i <= n; i++) {
        *ptr *=i;
    }
    cout << "Factorial: " << f << endl;
    return 0;
}