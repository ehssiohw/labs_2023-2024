/*Обменять значения двух переменных через указатели*/
#include <iostream>
using namespace std;
int main () {
    int b, tmp, a;
    int *ptr1;
    int *ptr2;
    cout << "Enter the first number: ";
    cin >> b;
    ptr1 = &b; //присваиваем адрес n в ptr1
    tmp = *ptr1; //вписываем старое значение числа
    cout << "Enter the second number: ";
    cin >> a;
    ptr2 = &a; //присваиваем второе число адрес в птр2
    *ptr1 = *ptr2; //обмен
    *ptr2 = tmp; //присвоили старое значение из 11 стррки

    cout << "Now the first is " << *ptr1 << endl;
    cout << "Now the second is " << *ptr2 << endl;
    return 0;
}