/*Найти минимальное и максимальное число в последовательности через указатели*/
#include <iostream>
using namespace std;
int main () {
    int n, tmp, *ptr=&tmp, max, min; 
    cout << "Enter the count of numbers: ";
    cin >> n;
    cout << "Enter the 1nd number: ";
    cin >> *ptr;
    max = *ptr;
    min = *ptr;
    for (int i = 2; i <= n; i++) {
        cout << "Enter the 2nd number: "
        cin >> *ptr;
        if (*ptr > max) {
            max = *ptr;
        }
        else if (*ptr < min) {
            min = *ptr;
        }
        cout << "Min: " << min << endl;
        cout << "Max: " << max << endl;
    }
    return 0;
}