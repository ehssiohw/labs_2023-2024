//посчитать произведение N первых натуральных чисел.
#include <iostream>

using namespace std;

int
main ()
{
  int N, pr;
  cin >> N;
  int i = 1;
  pr = 1;
  while (i <= N)
    {
      pr *= i;
      i++;

    }
  cout << pr << endl;
  return 0;
}
