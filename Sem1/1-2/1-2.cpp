/*создать программу для деления двух чисел (одно на другое). Предусмотреть, что они могут быть равными 0 (и на ноль делить нельзя).*/

#include <iostream>

using namespace std;

int main()
{
  float a,b;
  cin >> a >> b;
  if (a>0 && b>0) {
      cout << a/b << endl;
      return 0;
  }
  else if ((a==0 || b==0) && (a!=0 || b!=0)){
      cout << a/b << endl;
      return 0;
  }
  else {
    cout << "На 0 делить нельзя!" << endl;
    return 0;
  }
}

