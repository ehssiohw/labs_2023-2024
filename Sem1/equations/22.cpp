//Половинное деление e^x - e^(-x) - 2 = 0. Отрезок, содержащий корень [0; 1]
//e = 2,718
#include <iostream>
#include <cmath>
using namespace std;
int main () {
    int N;
    cout << "Enter a variant would you like. \n 1 - bisection method; \n 2 - Newton's method; \n 3 - Iteration method" << endl;
    cin >> N;
    
    switch (N) {
        
        case 1:{ 
        
        float a = 0, b = 1, c, eps = 0.000001;
        float fA, fB, fC;
        while (abs(b-a)>eps) {
        c = (a + b)/2;
        fA = exp(a) - exp(-a) - 2;
        fB = exp(b) - exp(-b) - 2;
        fC = exp(c) - exp(-c) - 2;
        if (fA * fC < 0) { b = c; }
        else if (fC * fB < 0) { a = c; }
        else { cout << "Something happened" << endl; }
    }
    cout << "Roots: " << a << " and " << b << endl; 
        break;
        }
        
        case 2:{
        //f(x) = e^x - e^-x - 2;
        //f'(x) = e^x + e^-x
        //f''(x) = e^x - e^-x
        // [0;1]
        // проверяем, а или б берется за начальное х: f(b) * f''(b) > 0
        // (e - 1/e - 2) * (e - 1/e) > 0 → x0 = b
        float x = 1, xpred = 0, eps = 0.000001; // x0 = b = 1; xpred = x(n-1);
        while (abs(x - xpred) > eps) {
            xpred = x;
            x = xpred - (exp(xpred) - exp(-xpred) - 2) / (exp(xpred) + exp(-xpred)); // xpred - f(xpred) / f'(xpred)
        }
        cout << "Root: " << x << endl; 
        break;
       }

        case 3:{
        //f(x) = e^x - e^-x - 2;
        //f'(x) = e^x + e^-x
        //f''(x) = e^x - e^-x
        //[0;1]  
        //ф(x) = x + lam(exp(x) - exp(-x) - 2)
        // -1/r < lam < 0: если f'(x) > 0
        // 0 < lam < 1/r: если f'(x) < 0
        //r = max(|f'(a)|, |f'(b)|) = max(1 + 1, e + 1/e) = max(2, 3) = 3
        // f'(x) > 0 → -1/r < lam < 0 (-1/3 < lam < 0)
        //берем любое удобное число из диапазона выше: lam = -0.2
        double x = 1, xpred = 0, lam = -0.2, eps = 0.000001; //x из [0,1]; xpred < x;
        while (abs(x-xpred) > eps) {
            xpred = x;
            x = lam * (exp(xpred) - exp(-xpred) - 2) + xpred; // x = lam * f(x) + xpred
        }
        cout << "Root: " << x << endl;  
        break;
        }
    }
return 0; 
}
