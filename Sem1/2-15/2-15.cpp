#include <iostream>
using namespace std;

int main() {
    int n;
    int s;
    cout << "Enter a natural number: ";
    cin >> n;
    cout << "Enter the number to find: ";
    cin >> s;
    bool f = false;

    for (int i = 0; i < 5; i++) {
        if (n == s) {
            f = true;
            break;
        }
    }

    if (f) {
        cout << "Digit " << s << " in natural number" << endl;
    } else {
        cout << "Digit " << s << " not in natural number" << endl;
    }
    return 0;
}
