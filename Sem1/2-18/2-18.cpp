#include <iostream>
#include <string>
#include <algorithm> 
using namespace std;
int main () {
    int n;
    cout << "Enter the natural number: ";
    cin >> n;
    string intstr = to_string(n);
    reverse(intstr.begin(), intstr.end()); 
    cout << intstr; 
    return 0;
}