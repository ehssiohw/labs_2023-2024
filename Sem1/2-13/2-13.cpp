#include <cmath>
#include <iostream>
using namespace std;
int main (){
int a,b,c;
cout << "Enter odds a,b,c: ";
cin >> a >> b >> c;
double d = b*b-4*a*c;
if (d < 0) {
    cout << "D < 0. No roots";
}
else if (d == 0) {
    double x = -b/2*a;
    cout << "D = 0. Single root: " << x;
}
else {
    double x1 = (-b+sqrt(d))/2*a;
    double x2 = (-b-sqrt(d))/2*a;
    cout << "D > 0. The first root: " << x1 << "The second root: " << x2;
    
}
return 0;
}
