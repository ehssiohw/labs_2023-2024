//S = 0+1+2-3+4+5-6+7+8-9 ====
#include <iostream>
using namespace std;
int main () {
    int n, sum;
    cout << "Enter a count of terms: ";
    cin >> n;
    sum = 0;
    for (int i = 1; i <= n; i++ ){
        if (i%3==0){
            sum -=i;
        }
        else {
            sum +=i;
        }
        
    }
    cout << "Sum: " << sum;
    return 0;
}