#include <iostream>
#include <vector>
using namespace std;
int main () {
    int n;
    cout << "Enter a count of numbers: ";
    cin >> n;
    vector <int> V;
    if (n <= 0) {
        cout << "You need in another number (n > 0): " << endl;
    }
    else {
        for (int i = 0; i < n; i++) {
            int k;
            cout << "Enter the " << i+1 << "number: ";
            cin >> k;
            V.push_back(k); //add and go to end (like an array)
            
        }
        int max = V[0];
        for (int i = 1; i < n; i++){
            if (V[i] > max) {
                max = V[i];
                break; //when we found max, we don't need the other. This is max
            }
        }
      cout << "Max number is " << max << endl;  
      return 0;
    }
}
