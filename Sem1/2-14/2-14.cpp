#include <iostream>
using namespace std; 
int main () {
    int n, sum, tmp;
    cout << "Enter a natural number: ";
    cin >> n;
    sum = 0;
    tmp = n;
    while (tmp > 0){
        sum += tmp %10;
        tmp /= 10;
    }
    cout << "Sum: " << sum << endl;
    return 0;
    
}
//(n % 10) + (n % 100 / 10) + (n % 1000 / 100)
